# Geant 4 simulation of FoCAL-H testbeam 

This is a pure Geant 4 implementation of a simulation of the FoCAL
test-beam setup.   A similar simulation, based on VMC, can be found in
the top-level sub-directory [`vmc`](../vmc).

## Content 

General comment: 

- File pairs ending in `.cc` and `.hh` or single files ending in `.hh`
  are part of the simulation code that will be compiled into a single
  application (`main`).
- Files ending in `.C` are ROOT scripts that are generally ACLiC
  compiled by ROOT.  Some of these files, however, are used as
  declaration files by the compiled application. 
- Not all source files are present in this directory.  Other files,
  shared with the [VMC](../vmc) sister-project are in the top-level
  sub-directory [`common`](../common).  This includes some data
  structure declarations. 

### Geometry building 

- [`Builder.hh`](Builder.hh) and [`Builder.cc`](Builder.cc) builds the
  detector setup.  This uses [`FocalHBuilder`](FocalHBuilder.hh) and
  [`FocalEBuilder`](FocalEBuilder.hh). 
- [`FocalHBuilder.hh`](FocalHBuilder.hh) and
  [`FocalHBuilder.cc`](FocalHBuilder.cc) builds the FoCAL-H geometry
  and registers sensitive detectors for the FoCAL-H. 
- [`FocalEBuilder.hh`](FocalEBuilder.hh) and
  [`FocalEBuilder.cc`](FocalEBuilder.cc) builds the FoCAL-E geometry
  and registers sensitive detectors for the FoCAL-E. 

### Data structures 

- [`Hit.C`](Hit.C) base class for hit data structures 
- [`FocalHHit.C`](FocalHHit.C) a hit in FoCAL-H.  Encodes energy loss,
  particle type, particle momentum, position, simulated light
  yield in the hit step, and the straw identifier. The objects contain 
  - Track number that caused the hit 
  - PDG identifier of the particle type
  - Spatial location of the hit $`(x,y,z,t)`$
  - Momentum of the particle at the hit $`(p_x,p_y,p_z,E)`$
  - Energy loss 
  - Model of light-yield from this step 
  - The volume number of the straw in which the hit was produced. 
- [`FocalEHit.C`](FocalEHit.C) a hit in FoCAL-E.  Encodes energy loss,
  particle type, particle momentum, position, and pad or pixel
  identifier.  The objects contain 
  - Track number that caused the hit 
  - PDG identifier of the particle type
  - Spatial location of the hit $`(x,y,z,t)`$
  - Momentum of the particle at the hit $`(p_x,p_y,p_z,E)`$
  - Energy loss 
  - Model of light-yield from this step 
  - The volume number of the pad or pixel in which the hit was
	produced.
- [`FocalHSum.C`](FocalHSum.C) an integrated hit in each of the
  FoCAL-H straws.  This records the straw number, summed energy loss,
  summed light-yield, and number of photons that exit the straw
  through the back. The objects contain 
  - Total energy loss in a straw
  - Number of hits that contributed to the energy loss
  - Total simulated light yield in a straw 
  - Number of photons exiting the straw at the end
  - Sum of kinetic energy of photons exiting the straw at the end 
- [`FocalHFlux.C`](FocalHFlux.C) records the type of particles that
  exit the FoCAL-H volume through the back.  This encodes the particle
  type and whether the particle entered or left the volume. The
  objects contain
  - Track number that caused the hit 
  - PDG identifier of the particle type
  - Spatial location of the hit $`(x,y,z,t)`$
  - Momentum of the particle at the hit $`(p_x,p_y,p_z,E)`$
  - Flags which says whether track entered or exited the set-up.
- [`RootIO.hh`](RootIO.hh) and [`RootIO.cc`](RootIO.cc) - a singleton
  wrapper around `EventTree` that allows global access to an
  `EventTree` object.  This is used in `OnEvent` and sensitive
  detectors.  The main program sets up the singleton.
- [`EventTree.C`](EventTree.C) - defines the class `EventTree`.  This
  is a manager of the ROOT event tree and has methods for creating
  hits and the like. 
  
### Sensitive detectors 

- [`FocalHHitter.hh`](FocalHHitter.hh) and
  [`FocalHHitter.cc`](FocalHHitter.cc)  creates hits on the output
  ROOT `TTree` for FoCAL-H.
- [`FocalEEitter.hh`](FocalEEitter.hh) and
  [`FocalEEitter.cc`](FocalEEitter.cc)  creates hits on the output
  ROOT `TTree` for FoCAL-E.
- [`FocalHSummer.hh`](FocalHSummer.hh) and
  [`FocalHSummer.cc`](FocalHSummer.cc) creates sum hits on the output
  ROOT `TTree` for FoCAL-H.  Sum hits are per straw, and include a
  modelled light yield and the number of photons that exited the
  straws at the back. 
- [`FocalHFluxer.hh`](FocalHFluxer.hh) and
  [`FocalHFluxer.cc`](FocalHFluxer.cc) records which particles exited
  the FoCAL-H volume through the back.  This can be used to study the
  flux of for example neutrons through the back of the detector. 
- [`FrontBack.hh`](FrontBack.hh) and [`FrontBack.cc`](FrontBack.cc) is
  a utility base class for `FocalHFluxer` and `FocalHSummer` to detect
  when a particle enters or exits the FoCAL-H volume. 
  
### Steering of simulation

- [`OnRun.hh`](OnRun.hh) - defines the class `OnRun`.  In this setup,
  this does nothing.  Normally it would do stuff at start and end of
  run.
- [`OnEvent.hh`](OnEvent.hh) and [`OnEvent.cc`](OnEvent.cc) - defines
  the class `OnEvent`.  Objects of this class is responsible for
  clearing event storage before the start of an event, and flushing
  event data to disk at the end of the event.  This uses the singleton
  `RootIO` for this.
- [`OnStack.hh`](OnStack.hh) and [`OnStack.cc`](OnStack.cc) records
  all tracks - primary and secondary - in a branch of the output ROOT
  `TTree`.  Not all tracks will be saved to disk - only those marked
  for saving will be stored - and possibly their ancestors. 
- [`Generator.hh`](Generator.hh) and [`Generator.cc`](Generator.cc) -
  defines the class `Generator`.  This is responsible for generating
  the primary particle of an event.  This uses a simple particle gun
  and starts at a vertex smeared in the transverse plane by a normal
  distribution.
- [`OnInit.hh`](OnInit.hh) - defines the class `OnInit`.  An object of
  this class initialises objects of the classes `Generator`,
  `OnEvent`, `OnRun`, and `OnStack`.  Other than that, it plays no
  role.
- [`main.cc`](main.cc) - the main program.  The program accepts a lot
  of options to configure the simulation.  Pass the option `--help` to
  see a summary of these options.
  
### Other 

- [`macros`](macros) - this directory contains example macros.  The
  `main` program is set-up to read macros from this directory. 
- [`Makefile`](Makefile) - recipes to build the code. 
- [`README.md`](README.md) - this file 


## Building 

Simply do 

	make build
	
To clean up, do 

	make clean 
	
This will remove all generated files, _except_ ROOT files.  To clean
these too, do 

	make realclean 

Note, Geant4 [has a
problem](https://bugzilla-geant4.kek.jp/show_bug.cgi?id=2242)  with
Wayland (the default GUI in most modern Linux distributions) and Qt.
Make sure you disable Qt when building Geant4. 

## Running 

_Do not sh*t were you eat_

The above is generally a good advice, also in the case of software.
What it means is that we do not put our messy stuff with the stuff we
want to work with.  That is, we want to keep our sources and the data
we generate with the sources separate from each other.  To that end,
this project provides the [`data`](data) sub-directory.   Change
directory to that sub-directory, and you should run the code from
there.

The [`data/Makefile`](data/Makefile) contains a number of targets to
help use the code.  Do

	make help 
	
to see more.

If you prefer, you can also run simulations _by-hand_.  For example 

	../main 
	
If you want to use a macro, e.g.,
[`macros/run.mac`](macros/run.mac) to set-up the job,
do

	../main -m run.mac 
	
(the sub-directory [`macros`](macros) is in the search path).

You can also pass options to control the simulation.  For example, 

	../main -n 100 -p 200 -t pi- 
	
to generate 100 events of $`\pi^-`$ with
$`p_z=200\,\mathrm{GeV}/\mathrm{c}`$.  Note, if you specify the number
of events with the option `-n` then _no_ macros are read.

You can also start an interactive session with 

	../main -i 
	
For more on the various options available, do 

	../main --help 
	
The default output file is `events.root`, which can be processed via
the class `EventTree` - see for example the ROOT script
[`Read.C`](Read.C).  Make sure you load the class definitions before
processing the data.  For example

	root Load.C Read.C 
	
(The [`data/.rootrc`](data/.rootrc) is set up so that ROOT will look
for scripts in the parent directory).

## Timing 

Running this example for 100 $`\pi^-`$ with
$`p_z=200\,\mathrm{GeV}/\mathrm{c}`$ gives 

    Command being timed: "../main -t pi- -p 200 -n 100"
    User time (seconds): 183.12
    System time (seconds): 0.31
    Percent of CPU this job got: 99%
    Elapsed (wall clock) time (h:mm:ss or m:ss): 3:03.62

or roughly 1.8 seconds per event. 

## Profiling 

To run the code with profiling, do for example 

	make 200_100_events.root PROFILE=1
	
and then investigate the profiling output with for example `hotspot`. 

  
## Run attenuation post step 

In `data` 

    root -l LoadData.C Attenuate.C\(-1,\"hits.root\"\) 
    
Note, this code does not take the rotation of the detector into
account. 


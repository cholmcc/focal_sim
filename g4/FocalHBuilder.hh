//____________________________________________________________________ 
//  
//  FoCAL Geant4 Simulation
//  Copyright (C) 2023 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    FocalHBuilder.hh
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon 06 Mar 2023 09:25:16 PM CET
    @brief   Focal-H Geometry builder 
 */
#ifndef __FocalHBuilder__
#define __FocalHBuilder__
#include <G4SystemOfUnits.hh>
#include "FocalH.C"
#include <map>

class G4VPhysicalVolume;
class G4LogicalVolume;
class G4NistManager;
class G4Material;
class G4AssemblyVolume;
class G4Region;
class G4VSensitiveDetector;

//====================================================================
class FocalHBuilder
{
public:
  FocalHBuilder(const FocalH& param)
    : fParameters(param)
  {}
  /** @{
      @name Interface methods */
  /** Create needed materials for the FocalH */
  void CreateMaterials(G4NistManager* manager);
  /** Build the actual volume */
  G4LogicalVolume* Construct(G4NistManager* nistm);
  /** Create the senstive detectors needed */
  std::map<G4VSensitiveDetector*,G4LogicalVolume*> CreateSensitive();
  /** @} */
  /** @{
      @name Get info methods */
  /** Get scinitilator volume */
  G4LogicalVolume*   GetScint() const { return fScintVolume; }
  /** Get flux volume */
  G4LogicalVolume*   GetFlux()  const { return fFluxVolume; }
  /** Get length of cobber tubes */
  double   GetLength()     const { return fParameters.GetFluxLength() * cm; }
  /** Get width of cobber tubes */
  double   GetWidth()      const { return fParameters.GetWidth() * cm; }
  /** Get height of cobber tubes */
  double   GetHeight()      const { return fParameters.GetHeight() * cm; }
  /** Get detector specific region */
  G4Region* GetRegion()    const { return fRegion; }
  /** Get the parameters */
  const FocalH& GetParameters() const { return fParameters; }
  /** @} */
protected:
  /** Scintilator logical volume */
  G4LogicalVolume* fScintVolume;
  /** Flux volume */
  G4LogicalVolume* fFluxVolume;
  /** The Region of this sub-detector */
  G4Region*        fRegion;
  /** Parameters.  Const reference so we may not change it */
  const FocalH&    fParameters;
};

#endif
//
// EOF
//

  
  
    

# Common code (source-level) between VMC and G4

The code in this directory is shared - source-level - between many of
the sub-projects.  These are placed here so we only need to update
them once.  The idea is to keep as much as possible of the code base
common, so that we can be more certain that all simulations simulate
the same thing.

Also, since the output of both the [VMC](../vmc) and [Geant 4](../g4)
are compatible (on the source-level, not binary-level), we can use the
same code to analyse the outputs (e.g., digitisation).  However, since
that code will need to include - literally - different data structure
definitions (e.g., [VMC `FocalHHit`](../vmc/FocalHHit.C) is not the
same as [Geant 4 `FocalHHit`](../g4/FocalHHit.C)), those files are put
in this sub-directory.  Code that is compatible both on source and
binary level is put in the [`common`](../common) sub-directory.

## Content

- [`Leaving.C`](Leaving.C) investigate the simulation output for
  particle types that exit the FoCAL-H sub-detector volumes through
  the back. 
- [`Profiler.C`](Profiler.C) investigate the simulation output for
  where in the FoCAL-H energy is deposited. 
- [`FocalHDigitizer.C`](FocalHDigitizer.C) digitises the simulation
  output from FoCAL-H into something similar to the actual data.  The
  parameters of the digitisation is managed through the
  [`FocalHRO`](../common/FocalHRO.C) class. 
- `FocalEDigitizer.C` (TBD) digitises the simulation output from
  FoCAL-E into something similar to the actual data. 
  
  

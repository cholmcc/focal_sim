#include "PrimaryGenerator.hh"
#include "G4ParticleTable.hh"
#include "G4ParticleGun.hh"
#include "G4SystemOfUnits.hh"
#include "Randomize.hh"

//--------------------------------------------------------------------
PrimaryGenerator::PrimaryGenerator()
  : fpParticleGun_1(0)
{
  // Particle table
  G4ParticleTable* particleTable = G4ParticleTable::GetParticleTable();

  // 1st gun - setup for fixed parameters
  fpParticleGun_1  = new G4ParticleGun();

  G4String particleName = "e+";
  G4double momentum = 150.0*GeV;
  G4ParticleDefinition* particle = particleTable->FindParticle(particleName);
  
  fpParticleGun_1->SetParticleDefinition(particle);
  fpParticleGun_1->SetParticleMomentum(momentum);   //kinetic energy define
}

//--------------------------------------------------------------------
PrimaryGenerator::~PrimaryGenerator()
{
  delete fpParticleGun_1;
}

//--------------------------------------------------------------------
void PrimaryGenerator::GeneratePrimaries(G4Event* anEvent)
{
  // Gun position - randomization
  G4double offset1 = 40.0*(G4UniformRand()-0.5)*mm;
  G4double offset2 = 40.0*(G4UniformRand()-0.5)*mm;
  G4ThreeVector position_1 = G4ThreeVector(offset1, offset2, 0.0);

  G4ThreeVector momentumDirection = G4ThreeVector(0.0, 0.0, 1.0);
  fpParticleGun_1->SetParticleMomentumDirection(momentumDirection);

  fpParticleGun_1->SetParticlePosition(position_1);
  fpParticleGun_1->GeneratePrimaryVertex(anEvent); 
}
//
// EOF
//

//____________________________________________________________________ 
//  
//  FoCAL Geant4 Simulation
//  Copyright (C) 2023 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    OnStack.cc
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon 06 Mar 2023 09:25:16 PM CET
    @brief   Trigger when new tracks being pushed on the stack 
 */
#include "OnStack.hh"
#include "RootIO.hh"
#include <TClonesArray.h>
#include <TParticle.h>
#include <G4Track.hh>
#include <G4ThreeVector.hh>
#include <G4ParticleDefinition.hh>
#include <G4SystemOfUnits.hh>

//____________________________________________________________________
OnStack::OnStack()
  : G4UserStackingAction(),
    fParticles(0)
{}
//____________________________________________________________________
G4ClassificationOfNewTrack
OnStack::ClassifyNewTrack(const G4Track* track)
{
  // Geant4 track numbers start at 1!!!!
  G4int                pdg          = track->GetDefinition()->GetPDGEncoding();
  G4int                trackNo      = track->GetTrackID();
  G4int                parent       = track->GetParentID();
  G4int                status       = track->GetTrackStatus();
  G4ThreeVector        position     = track->GetPosition();
  G4double             time         = track->GetGlobalTime();
  G4ThreeVector        momentum     = track->GetMomentum();
  G4double             energy       = track->GetTotalEnergy();
  //const G4ThreeVector& polarisation = track->GetPolarization();

  RootIO::Instance()->AddParticle(trackNo,
				  pdg,
				  status,
				  parent,
				  position.x(),
				  position.y(),
				  position.z(),
				  time,
				  momentum.x(),
				  momentum.y(),
				  momentum.z(),
				  energy);
  // G4cout << pdg << " "
  // 	 << trackNo << " "
  // 	 << parent << " "
  // 	 << position << " "
  // 	 << time << " "
  // 	 << momentum << " "
  // 	 << energy << " "
  // 	 << polarisation << G4endl;
  return G4UserStackingAction::ClassifyNewTrack(track);
}
//____________________________________________________________________
//
// EOF
//

# FoCAL simulation code 

## Content

- [`common`](common) Code (source and binary) shared between `vmc`,
  `g4`, and `tb`.  These are mainly data and parameter classes.  
  
- [`shared`](shared) Source code that is shared between `vmc` and `g4`
  but which needs to be compiled separately. Therefore there are
  symbolic links in `vmc` and `g4` to files in this sub-directory, so
  that the ACLiC compiled code goes into the `vmc` and `g4`
  sub-directories.
  
- [`vmc`](vmc) A ROOT Virtual Monte-Carlo implementation of the
  simulation.  This can use Geant 3.21 and Geant 4 as back-ends. 
  
- [`g4`](g4) A pure Geant 4 implementation of the simulation. 

- [`tb`](tb) Investigating test-beam data 

- [`misc`](misc) Other stuff, like notes and such. 

## Dependencies 

The following is the requirements for the full project.  If you only
need to read test-beam data, all you need is **ROOT** and **ROOT
Framework** 

- [**ROOT** (>= 6.22.8)](https://root.cern.ch)
- [**ROOT VMC**](https://github.com/vmc-project/vmc)
- [**ROOT Framework** (>=
  0.20)](https://gitlab.cern.ch/cholm/root-framework) 
- [**ROOT Simulation** (>=
  0.20)](https://gitlab.cern.ch/cholm/root-simulation)
- For the different back-ends 
  - **GEANT 3.21**
    - [GEANT 3.21 VMC](https://github.com/vmc-project/geant3)
  - **Geant 4**
    - [Geant 4 >= 11.1.1](https://github.com/Geant4/geant4.git)
	- [VGM](https://github.com/vmc-project/vgm) 
	- [Geant 4 VMC](https://github.com/vmc-project/geant4_vmc.git)

The code should be built in the above order (except **Geant 4** can be
built parallel to **ROOT**), and make sure that 

- **VGM** is configure for **Geant 4** and **ROOT** 
- **Geant 4 VMC** is configured for **VGM** 
- The same C++ standard is used throughout 

The best option is to configure all projects to install in the same
_prefix_ 

- For **ROOT**, **ROOT VMC**, **GEANT 3.21 VMC**, **Geant 4**,
  **VGM**, and **Geant 4 VMC**

      $ cmake CMAKE_INSTALL_PREFIX=/my/prefix/location ... 
	  
- For **ROOT Framework** and **ROOT Simulation** 

      $./configure --prefix=/my/prefix/location ...

To do parallel execution of simulation, you also need [**GNU
parallel**](https://gnu.org/software/parallel).   To install on Debian
and derivatives (e.g. Ubuntu), do 

    $ sudo apt install parallel 

See also [`Build.md`](Build.md).

## Usage 

For example 

	$ cd vmc/data 
	$ make g3_production 
	$ make g4_production 
	
or 

	$ cd g4/data
	$ make production
	
In all cases the output is stored in ROOT `TTree` objects. 

## Test beam data 

See the [`tb`](tb) sub-directory. 


# Common code (source and binary) between VMC and G4

The code in this directory is shared - both in source and binary
form - between many of the sub-projects.  These are placed here so we
only need to update them once.  The idea is to keep as much as
possible of the code base common, so that we can be more certain that
all simulations simulate the same thing. 

Also, since the output of both the [VMC](../vmc) and [Geant 4](../g4)
are compatible (on the source-level, not binary-level), we can use the
same code to analyse the outputs (e.g., digitisation).  However, since
that code will need to include - literally - different data structure
definitions (e.g., [VMC `FocalHHit`](../vmc/FocalHHit.C) is not the
same as [Geant 4 `FocalHHit`](../g4/FocalHHit.C)), those files are put
into the [`shared`](../shared) sub-directory and linked to from the
specific sub-projects.

## Content 

### Used throughout 

- [`FocalE.C`](FocalE.C) parameters of the FoCAL-E detector 
- [`FocalH.C`](FocalH.C) parameters of the FoCAL-H detector 

### For simulations 

- [`AirOptical.C`](AirOptical.C) definition of optical properties of
  air
- [`ScintOptical.C`](ScintOptical.C) definition of optical properties
  of scintillators (PolyPherene).

### For analysis (second pass)

- [`AnaConfig.C`](AnaConfig.C) definition of analysis (second pass)
  jobs.
- [`Analyse.C`](Analyse.C) steering macro for analysis (second pass)
  jobs.   
- [`Analyser.C`](Analyser.C) base class for analyser (second pass)
  tasks. 
- [`FocalHDigit.C`](FocalHDigit.C) a digit (signal) in FoCAL-H. Also
  serves as base class for digitised FoCAL-H simulated responses
  [`FocalHSDigit.C`](FocalHSDigit.C).
- [`FocalHSDigit.C`](FocalHSDigit.C) digitised FoCAL-H simulated
  responses. 
- [`FocalHRO.C`](FocalHRO.C) FoCAL-H read-out parameters, used for
  digitisation of simulated response. 
- [`LoadAna.C`](LoadAna.C) load analysis classes into a ROOT session.


  

#
# The files
#
cmndir		:= $(srcdir)../common/
shrdir		:= $(srcdir)../shared/
DATA_SOURCES	:= $(srcdir)Hit.C			\
		   $(cmndir)FocalH.C			\
		   $(cmndir)FocalE.C			\
		   $(srcdir)FocalHHit.C			\
		   $(srcdir)FocalEHit.C			\
		   $(cmndir)FocalHDigit.C		\
		   $(cmndir)FocalHSDigit.C		\
		   $(srcdir)FocalHFlux.C		\
		   $(srcdir)FocalHSum.C			\
		   $(srcdir)EventTree.C			

ANA_SOURCES	:= $(cmndir)Analyse.C			\
		   $(cmndir)Analyser.C			\
		   $(shrdir)Profiler.C			\
		   $(shrdir)Leaving.C			\
		   $(shrdir)FocalHDigitizer.C

ATT_SOURCES	:= $(cmndir)Attenuate.C			\
		   $(shrdir)Attenuator.C		\
		   $(cmndir)AttConfig.C			\
		   $(cmndir)LoadAtt.C			

SIM_SOURCES	:= $(srcdir)main.cc			\
		   $(srcdir)RootIO.cc			\
		   $(srcdir)FocalEBuilder.cc		\
		   $(srcdir)FocalHBuilder.cc		\
		   $(srcdir)Builder.cc			\
		   $(srcdir)Generator.cc		\
		   $(srcdir)OnStack.cc			\
		   $(srcdir)OnEvent.cc			\
		   $(srcdir)Fast.cc			\
		   $(srcdir)FrontBack.cc		\
		   $(srcdir)FocalHHitter.cc		\
		   $(srcdir)FocalEHitter.cc		\
		   $(srcdir)FocalHSummer.cc		\
		   $(srcdir)FocalHFluxer.cc

SIM_HEADERS	:= $(srcdir)FocalEBuilder.hh		\
		   $(srcdir)FocalHBuilder.hh		\
		   $(srcdir)Builder.hh			\
		   $(srcdir)Generator.hh		\
		   $(srcdir)OnInit.hh			\
		   $(srcdir)OnRun.hh			\
		   $(srcdir)OnStack.hh			\
		   $(srcdir)OnEvent.hh			\
		   $(srcdir)Fast.hh			\
		   $(srcdir)FrontBack.hh		\
		   $(srcdir)FocalHHitter.hh		\
		   $(srcdir)FocalEHitter.hh		\
		   $(srcdir)FocalHSummer.hh		\
		   $(srcdir)FocalHFluxer.hh		\
		   $(srcdir)RootIO.hh			\
		   $(DATA_SOURCES)




#
#  EOF
#

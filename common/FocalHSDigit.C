//____________________________________________________________________ 
//  
//  FoCAL Geant4 Simulation
//  Copyright (C) 2023 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    FocalHSDigit.C
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon 06 Mar 2023 09:18:28 PM CET
    @brief   Information about particle energy loss in the FoCAL-H
    scintilator fibres.  */
#ifndef __FocalHSDigit__
#define __FocalHSDigit__
#include "FocalHDigit.C"

/** A digit in the Focal-H scintilator fibres.  The detector coordinates
    are encoded into the copy number and can be decoded by member
    functions.  The local X,Y coorinates of the scintilating fibres
    can also be retrieved, given the radius of the straws and the
    number of columns and rows.

    This derived class also stores the energy loss and possibly light
    yield in addition to the simulated ADC counts.
*/
class FocalHSDigit : public FocalHDigit
{
public:
  /** Default constructor */
  FocalHSDigit() : FocalHDigit(), fEloss(0), fLight(0) {}
  /** Construct from Lorentz vectors */
  FocalHSDigit(UShort_t              mcol,
	       UShort_t              mrow,
	       UShort_t              col,
	       UShort_t              row,
	       UInt_t                count=0,
	       Double_t              eloss=0,
	       Double_t              light=0)
    :  FocalHDigit(mcol,mrow,col,row,count),
       fEloss(eloss),
       fLight(light)
  {}
  /** Zero the internal data

   */
  void Clear(Option_t* option="")
  {
    FocalHDigit::Clear(option);
    fEloss = 0;
    fLight = 0;
  }
  /** add energy loss */
  void AddEloss(Double_t eloss) { fEloss += eloss; }
  /** add light yield */
  void AddLight(Double_t light) { fLight += light; }
  /** Add energy loss and light yield */
  void AddEloss(Double_t eloss, Double_t light) {
    AddEloss(eloss);
    AddLight(light);
  }
  /** Sum energy loss */
  Double_t fEloss;
  /** Sum light */
  Double_t fLight;

  ClassDef(FocalHSDigit,1);
};

#endif
//
// EOF
//


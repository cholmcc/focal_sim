To convert to ROOT, do 


    root LoadData.C ToDigits.C\(\"FILENAME\"\)

where FILENAME is one of the ".dat" files in the sub-directories.   You can do it all in a loop with 

    for fn in */*.dat ; do \
           root LoadData.C ToDigits.C\(\"$fn\"\) ; done


Or, perhaps 

    for fn in */*.dat ; do \
           make `dirname $fn`/`basename $fn .dat`.root 
		   
and for dists 

    for fn in */*.dat ; do \
           make `dirname $fn`/`basename $fn .dat`_dist.root 
		   

	

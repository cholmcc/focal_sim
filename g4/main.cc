//____________________________________________________________________ 
//  
//  FoCAL Geant4 Simulation
//  Copyright (C) 2023 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    main.C
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon 06 Mar 2023 09:25:16 PM CET
    @brief   The application main entry point
 */
#include "Builder.hh"
#include "FocalHBuilder.hh"
#include "FocalEBuilder.hh"
#include "OnInit.hh"
#include "RootIO.hh"
#include <G4RunManager.hh>
#include <G4UImanager.hh>
#include <G4UIExecutive.hh>
#include <G4VisExecutive.hh>
#include <G4GDMLParser.hh>
#include <G4FastSimulationPhysics.hh>
#include <G4ParticleDefinition.hh>
#include <QGSP_BERT.hh>
#include <G4OpticalPhysics.hh>
#include <G4EmStandardPhysics_option4.hh>
#include <G4OpticalPhoton.hh>
#include <G4OpticalParameters.hh>
#include <string>
#include <iostream>
#include <random>

void usage(const std::string& progname)
{
  auto& o = std::cout;
  o << "Usage: " << progname
    << " [-b MACRO] [common options]\n"
    << "       " << progname
    << " [-n NEV] [-t PARTICLE] [-p PZ] [-o OUTPUT]"
    << " [-s SEED1 SEED2] [-v LEVEL] [common options]\n"
    << "       " << progname << " [-i] [common options]\n\n" 
    << "Options:\n"
    << "  -h             Show this help\n\n"
    << "  Mode, one of\n"
    << "  -i             Interactive session - no batch macros executed\n"
    << "  -b MACRO       Execute macro MACRO - ignore '-n' options\n"
    << "  -n NEV         Generate NEV events - no macros executed\n\n"
    << "  Common options\n"
    << "  -o OUTPUT      Write ROOT output to OUTPUT file\n"
    << "  -D NCOL NROW   Set size of Focal-H\n"
    << "  -R OUTER INNER Set radii of absorber tube in centimetre\n"
    << "  -L LENGTH      Set length of detector in centimetre\n"
    << "  -E EPSILON     Set space between absorber and scintilator in centimetre\n"
    << "  -U TYPE        Set user interface type (Qt,Xm,[T]csh)\n"
    << "  -V VISTYPE     Visualisation type (Qt3D,TSG_XT_GLES,...)\n"
    << "  -O             Enable optical photons\n"
    << "  -G             Enable fast optical photons\n"
    << "  -H             Toggle make hits\n"
    << "  -S             Toggle make sums\n"
    << "  -F             Toggle make flux\n"
    << "  -T TIME        Max integration time in seconds\n"
    << "  -Q DEGREES     Rotation of Focal-H\n"
    << "  -Z             Disable Focal-E\n"
    << "  -W             Disable Focal-H\n"
    << "  -I PATH        Add path to macro search path\n"
    << "  --             Pass rest of command line to G4 UI\n\n"
    << "  Only for '-n' mode:\n"
    << "  -t PARTICLE    Set particle type produced\n"
    << "  -p PZ          Set particle Z-momentum in GeV/c\n"
    << "  -s SEED1 SEED2 Set random number seeds\n"
    << "  -v LEVEL       Set verbosity to LEVEL\n\n"
    << "If MACRO is specified, then '-n' options are ignored.\n"
    << "If '-i' is passed, no macros are read\n"
    << "If either SEED1==0 or SEED2==0, then set random seeds\n"
    << "VIZType can be one of\n"
    << "- ATree\n"
    << "- DAWNFILE\n"
    << "- HepRepFile\n"
    << "- RayTracer\n"
    << "- VRML2FILE\n"
    << "- gMocrenFile\n"
    << "- TSG_OFFSCREEN\n"
    << "- TSG_FILE\n"
    << "- OGLI\n"
    << "- OGLS\n"
    << "- OGLIXm\n"
    << "- OGLSXm\n"
    << "- OGLIX\n"
    << "- OGLSX\n"
    << "- RayTracerX\n"
    << "- Qt3D\n"
    << "- TSGX11\n"
    << "- TSGXt\n"
    << "- TSGQt\n"
    << std::endl;
}

int main(int argc, char** argv)
{
  std::string    session         = "Qt";
  std::string    vizdev          = "Qt3D";
  std::string    particle        = "pi+";
  std::string    output          = "events.root";
  double         pz              = 200;//GeV/c
  std::string    mainMacro       = "";
  bool           interact        = false;
  bool           optical         = false;
  bool           fast            = false;
  unsigned int   seed1           = 41241;
  unsigned int   seed2           = 87053;
  unsigned int   nev             = 100;
  int            verbose         = 0;
  bool           makeHits        = true;
  bool           makeSums        = true;
  bool           makeFlux        = true;
  bool           enableFocalH    = true;
  bool           enableFocalE    = true;
  double         maxTime         = 1e-6;//Seconds
  double         hcalAngle       = 3;
  std::vector<std::string> macros;
  std::vector<std::string> paths;
  
  
  std::ifstream rc("main.cc",std::ios::in);
  if (rc) {
    std::cerr << "Please do not sh*t where you eat!\n\n"
	      << "Execute the simulation _outside_ of the source "
	      << "directory - for example in the provided \"data\" "
	      << "sub-directory" << std::endl;
    return 1;
  }

  std::vector<char*> args;
  std::string        tmp1;
  std::string        tmp2;
  args.push_back(argv[0]);
  bool toSession = false;
  for (int i = 1; i < argc; i++) {
    std::string arg(argv[i]);
    if (arg == "--")  {
      toSession = true;
      continue;
    }
    if (toSession) {
      args.push_back(argv[i]);
      continue;
    }
    if (arg == "--help") {
      usage(argv[0]);
      return 0;
    }
    if (arg[0] == '-') {
      switch (arg[1]) {
      case 'i': interact  = !interact;                break;
      case 't': particle  = argv[++i];                break;
      case 'p': pz        = std::stof(argv[++i]);     break;
      case 'n': nev       = std::stoi(argv[++i],0,0); break;
      case 'b': mainMacro = argv[++i];                break;
      case 'o': output    = argv[++i];                break;
      case 'v': verbose   = std::stoi(argv[++i],0,0); break;
      case 's':
	tmp1 = argv[++i];
	if (tmp1.find(' ') != std::string::npos) {
	  size_t i = tmp1.find(' ');
	  tmp2 = tmp1.substr(i+1);
	  tmp1 = tmp1.substr(0,i);
	}
	else
	  tmp2 = argv[++i];
	seed1     = std::stoi(tmp1,0,0);
	seed2     = std::stoi(tmp2,0,0);
	break;
      case 'U': session        = argv[++i];            break;
      case 'O': optical        = !optical;             break;
      case 'G': fast           = !fast;                break;
      case 'H': makeHits       = !makeHits;            break;
      case 'S': makeSums       = !makeSums;            break;
      case 'F': makeFlux       = !makeFlux;            break;
      case 'T': maxTime        = std::stof(argv[++i]); break;
      case 'V': vizdev         = argv[++i];            break;
      case 'Z': enableFocalE   = !enableFocalE;        break;
      case 'W': enableFocalH   = !enableFocalH;        break;
      case 'Q': hcalAngle      = std::stof(argv[++i]); break;
      case 'I': paths.push_back(argv[++i]);            break;
      case 'h': usage(argv[0]);                        return 0;
      default:
	throw std::runtime_error("Unknown option " + arg);
      }
      continue;
    }
    macros.push_back(argv[i]);
  }
  if (seed1 == 0 || seed2 == 0) {
    std::random_device dev;
    seed1 = dev();
    seed2 = dev();
  }
  if (vizdev == "Qt3D" or session == "Qt") {
    vizdev  = "Qt3D";
    session = "Qt";
  }
  if (output == "auto") {
    std::stringstream s;
    s << std::setfill('0')
      << std::setw(3) << int(pz) << "_"
      << std::setw(5) << nev     << "_"
      << std::hex
      << "0x" << std::setw(8) << seed1 << "_"
      << "0x" << std::setw(8) << seed2 << ".root";
    output = s.str();
  }
  std::cout << "Output file set to \"" << output << "\"" << std::endl;

  FocalH focalHP;
  focalHP.fMaxTime       = maxTime;
  focalHP.fFast          = fast;
  FocalE focalEP;
  
  RootIO::Instance()->Open(output.c_str(), "T", "RECREATE");
  RootIO::Instance()->MakeParticles(true); // !optical || fast);
  RootIO::Instance()->MakePrimary(true);
  RootIO::Instance()->SetVerbose(verbose);
  
  G4GDMLParser    gp;
  G4RunManager*   rm     = new G4RunManager;
  // focalEP.fLayers            =   { 0, 1, 1, 0 }; 
  FocalHBuilder*  focalH = enableFocalH ? new FocalHBuilder(focalHP) : 0;
  FocalEBuilder*  focalE = enableFocalE ? new FocalEBuilder(focalEP) : 0;
  Builder*        geom   = new Builder(focalH,focalEP.GetLength()+15,
				       focalE,0,
                                       hcalAngle);
  
  G4VModularPhysicsList* pl = new QGSP_BERT;//FTFP_BERT // Which one to use
  if (optical || fast) {
    if (fast) {
      auto fsp = new G4FastSimulationPhysics();
      auto pn  = G4OpticalPhoton::OpticalPhotonDefinition()->GetParticleName();
      fsp->ActivateFastSimulation(pn);
      pl->RegisterPhysics(fsp);
      std::cout << "Register fast simulation for " << pn << std::endl;
    }

    std::cout << "***** Register optical physics ******" << std::endl;
    // pl->ReplacePhysics(new G4EmStandardPhysics_option4()); // Needed?
    pl->RegisterPhysics(new G4OpticalPhysics()); // Use predefined

    auto* opticalParams = G4OpticalParameters::Instance();    
    opticalParams->SetBoundaryInvokeSD(true);
    opticalParams->SetProcessActivation("Cerenkov",false);//Slow as F**k
    opticalParams->SetProcessActivation("Scintillation",true);
    opticalParams->SetProcessActivation("OpBoundary",true);
    opticalParams->SetProcessActivation("OpAbsorbtion",true);
    opticalParams->SetCerenkovTrackSecondariesFirst(true);
    opticalParams->SetScintTrackSecondariesFirst(true);
    //opticalParams->SetBoundaryVerboseLevel(5);//0);
  
  }
  
  rm->SetUserInitialization(geom);
  rm->SetUserInitialization(pl);
  rm->SetUserInitialization(new OnInit(geom));
  rm->Initialize();

  G4VisExecutive* vm = new G4VisExecutive;
  vm->Initialize();

  // User interface 
  G4UImanager* uu = G4UImanager::GetUIpointer();

  // Verbosity 
  std::map<std::string,int> levels = {
    {"/run/verbose",                          1},
    {"/tracking/verbose",                     2},
    {"/control/verbose",                      2},
    {"/event/verbose",                        2},
    {"/material/verbose",                     3},
    {"/process/verbose",                      3},
    {"/vis/verbose",                          4},
    {"/cuts/verbose",                         5},
    {"/geometry/navigator/verbose",           5},
    {"/particle/verbose",                     5},
    {"/particle/process/verbose",             5},
    {"/process/eLoss/verbose",                5},
    {"/process/had/verbose",                  5},
    {"/process/emp/verbose",                  5},
    {"/process/optical/verbose",              5},
    {"/process/optical/scintilation/verbose", 6},
    {"/process/optical/cerenkov/verbose",     6},
    {"/process/optical/boundary/verbose",     6},
    {"/process/optical/absorbtion/verbose",   6},
  };
  for (auto l : levels) 
    uu->ApplyCommand(l.first+" "+
                     std::to_string(std::max(verbose - l.second,0)));

  // Macro paths
  std::string mp = ".";
  for (auto p : paths) mp += ":" + p;
  uu->ApplyCommand("/control/macroPath "+mp);

  // Macros 
  for (auto m : macros) 
    uu->ApplyCommand("/control/execute " + m);
  
  if (interact) {
    G4UIExecutive* ui = new G4UIExecutive(args.size(),&(args[0]),session);
    std::vector<std::string> cmds{
      "/vis/open "+vizdev,
      "/vis/drawVolume world"};
    std::vector<std::string> ecmds{
      "/vis/scene/add/trajectories smooth",
      "/vis/scene/endOfEventAction accumulate",
      // "/vis/viewer/set/background white",
      "/vis/scene/add/axes",
      "/vis/viewer/set/style surface", // wireframe
      "/vis/viewer/set/edge true",
      "/vis/viewer/set/auxiliaryEdge true",
      "/vis/viewer/set/lineSegmentsPerCircle 100",
      "/vis/viewer/set/viewpointThetaPhi 120 150",
      ""
    };
    for (auto c : cmds)
      if (c != "") uu->ApplyCommand(c);
    if (vizdev != "Qt3D")
      for (auto c : ecmds)
        if (c != "") uu->ApplyCommand(c);
    
    ui->SessionStart();
    RootIO::Instance()->Close();
    return 0;
  }
  
  if (!mainMacro.empty()) {
    uu->ApplyCommand("/control/execute " + mainMacro);
    RootIO::Instance()->Close();
    return 0;
  }

  // Set seed 
  uu->ApplyCommand("/random/setSeeds "
		   +std::to_string(seed1)+" "+std::to_string(seed2));
  // Enable or disable sensitive detectors
  std::map<std::string,bool> sens = { {"FocalHHitter", makeHits&&enableFocalH},
				      {"FocalEHitter", makeHits&&enableFocalE},
				      {"FocalHSummer", makeSums&&enableFocalH},
				      {"FocalHFluxer", makeFlux&&enableFocalH}};
  for (auto s : sens) {
    std::cout << "Detector " << s.first << (s.second ? " " : " in")
	      << "active" << std::endl;
    uu->ApplyCommand("/hits/"
		     +std::string(s.second ? "" : "in")
		     +"activate "+s.first);
  }
  // Set particle 
  uu->ApplyCommand("/gun/particle " + particle);
  // Set energy
  uu->ApplyCommand("/gun/momentumAmp "+std::to_string(pz)+" GeV");
  // Run the simulation for nev events
  uu->ApplyCommand("/run/beamOn "+std::to_string(nev));

  RootIO::Instance()->Close();
  
  return 0;
}

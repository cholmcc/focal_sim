//____________________________________________________________________ 
//  
//  FoCAL Virtual Monte-Carlo Simulation
//  Copyright (C) 2023 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
#ifndef CONVERTER_C
#define CONVERTER_C
/** @file    Converter.C
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon 06 Mar 2023 09:18:28 PM CET
    @brief Convert ASCII test-beam data files to ROOT TTree with
    FocalHDigits.  */
#include <fstream>
#include <iostream>
#include <iomanip>
#include <string>
#include <map>
#include <array>
#include "FocalH.C"
#include "FocalHRO.C"
#include "FocalHDigit.C"
#include "FocalHDHeader.C"
#include <TFile.h>
#include <TTree.h>
#include <TClonesArray.h>
#include <TParameter.h>
#include <TMath.h>

/** Convert ASCII data file to other formats */
template <size_t NBOARD=3>
class BasicConverter
{
public:
  /** Type of trigger word */
  using Trigger = unsigned long long;
  /** Type channel ADCs  */
  using AdcArray = std::array<unsigned short,64>;
  /** Board data struture  */
  struct Board
  {
    /** Board number */
    unsigned short no  = 0;
    /** Trigger identifier */
    Trigger trg = 0;
    /** Time of trigger in micro-seconds */
    double ts  = 0;
    /** Array of ADC values */
    AdcArray adc;
    /** If this board was read */
    bool done = false;
  };
  /** Fixed size array of baords */
  using BoardArray = std::array<Board,NBOARD>;
  /** Map events to board information (yes, kept in memory - one can
      possibly investigate the current list of events and write out
      the events that are complete.  */
  using EventMap = std::map<Trigger,BoardArray>;
  /** Encode a board and ADC channel into unique identifier
      @param board Board number
      @param ch Channel number
   */
  static unsigned short BoardChannel(unsigned short board,
				     unsigned short ch)
  {
    return ((board & 0xFF) << 8) | (ch & 0x3F);
  }
  /** Type of mapping from board-channel to detector coordiante
      encoded */ 
  using BoardMap = std::map<unsigned short,unsigned int>;
  /** Record divider */
  constexpr static const char fDivider[] =
    "----------------------------------";
  /** Converter construction.

      @param filename Name of data file to read
      @param mapping Map from board/channel to
      module[col,row]/channel[col,row]
  */
  BasicConverter(const std::string& filename,
		 BoardMap&          mapping,
		 unsigned int       energy=0)
    : fIn(filename),
      fLineNo(0),
      fMapping(mapping),
      fLast(0),
      fEnergy(energy)
  {
    if (!fIn)
      throw std::runtime_error("Failed to open "+filename);

    // if (fEnergy == 0)
    //   //fEnergy = std::stoi(filename.substr(0,3));
    //   fEnergy = 60;

    if (!CheckMap())
      throw std::runtime_error("Invalid map");
  }
  /** Handle exception

      @param e Exception to handle 
   */
  void Handle(std::exception& e)
  {
    std::cerr << "At line " << std::setw(9) << fLineNo << std::endl;
    std::cerr << " while reading \"" << fLine << "\": " << std::endl;
    std::cerr << e.what() << std::endl;
  }
  /** Skip a record */
  void Skip()
  {
    do {
      GetLine();	    
      if (fLine.find(fDivider) == 0) break;
    } while (!fLine.empty());
  }
  /** Read in next line - skipping comments

      This keeps track of the current line number and stores the read
      line in the data member fLine.  Terminating newlines, etc. are
      removed.

      @return The line read 
   */
  std::string& GetLine()
  {
    while (!fIn.eof()) {
      std::getline(fIn, fLine);
      fLineNo++;
      //Info("","%9lld: %s", fLineNo,fLine.substr(0,fLine.size()-1).c_str());

      // std::cout << std::setw(9) << fLineNo << ": " << line << std::endl;
      // Ignore comment lines 
      if (fLine.find("//") == 0) {
	// std::cout << "Comment line" << std::endl;
	continue;
      }
      if (fLine[fLine.length()-1] == '\f' ||
	  fLine[fLine.length()-1] == '\n' ||
	  fLine[fLine.length()-1] == '\r')
	fLine.erase(fLine.length()-1);
      return fLine;
    }
    fLine = "";
    return fLine;
  }
  /** Read in the data

      @param maxTrig  The last trigger to read. 
   */
  void Read(Trigger maxTrig=0)
  {
    try {
      do {
	GetLine();
	
	// Check for board
	if (fLine.find("Board") == 0) {
	  // std::cout << "Board line" << std::endl;
	  unsigned int no = std::stoi(fLine.substr(5));
	  //Info("","%9lld: Board line => %u", fLineNo, no);
	  ReadBoard(no);

	  if (maxTrig > 0 and fLast > maxTrig) break;
	  continue;
	}
	if (fLine.empty()) break;
	
	throw std::runtime_error("Unknown line");
	
      } while (!fLine.empty());
    } catch (std::exception& e) {
      Handle(e);
      return;
    }
  }
  /** Read one (board) record

      @param no Board number to read. 
   */
  void ReadBoard(unsigned int no)
  {
    double        ts  = 0;
    Trigger       trg = 0xFFFFFFFFFFFFFFFF;
    do {
      GetLine();

      // Check for time 
      if (fLine.find("TS=") == 0) {
	// std::cout << "Time line" << std::endl;
	// Possiblue remove unit of micro seconds 
	auto us = fLine.rfind("us");
	if (us != std::string::npos) 
	  fLine.erase(us,fLine.size()-us);

	// Convert 
	ts = std::stod(fLine.substr(3));
	//Info("","%9lld: Got time %f", fLineNo, ts);
	continue;
      }

      // Check for trigger
      if (fLine.find("TrgID=") == 0) {
	trg = std::stoull(fLine.substr(6));
	//Info("","%9lld: Got trigger %llu", fLineNo, trg);
	continue;
      }

      // Check for channel header
      if (fLine.find("CH") == 0) {
	if (trg > fLast) {
	  if (trg % 10000 == 0) 
	    std::cout << "Trigger " << trg << std::endl;
	  fLast        = trg;
	}
	//Info("","%9lld: Got channel header %llu %u", fLineNo, trg, no);
	if (fEventMap.find(trg) == fEventMap.end()) {
	  // Warning("","%9lld: Trigger %llu not found in map", fLineNo, trg);
	  // fEventMap.insert(std::make_pair(trg,BoardArray{}));
	  // fEventMap.try_emplace(trg);
	}
	if (fEventMap[trg].size() <= no) {
	  Fatal("","%9lld: Board %u out of bounds in map for %llu",
		fLineNo, no, trg);
	}
	Board& board = fEventMap[trg][no];
	if (board.done) {
	  std::cerr << "Board " << no << " for trigger "
		    << trg << " already read, skip this record"
		    << std::endl;
	  Skip();
	  break;
	}

	board.no  = no;
	board.trg = trg;
	board.ts  = ts;
	ReadChannels(board);
	break;
      }

      // Check for record divider
      if (fLine.find(fDivider) == 0) {
	// std::cout << "Divider line" << std::endl;
	break;
      }

      throw std::runtime_error("while reading board - unknown line");
	  
    } while (!fLine.empty());
  }

  /** Read ADC values

      @param board Board data structure read the channel information
      into.
   */
  void ReadChannels(Board& board)
  {
    std::string line;
    do {
      line = GetLine();

      // Check for record end 
      if (line.find(fDivider) == 0) {
	// std::cout << "Divider line (end record)" << std::endl;
	board.done = true;
	break;
      }
      
      size_t sp = line.find(' ');
      if (sp == std::string::npos) 
	throw std::runtime_error("Malformed channel line");
      unsigned int ch  = std::stoi(line.substr(0,sp));
      unsigned int adc = std::stoi(line.substr(sp));
      board.adc[ch] = adc;
      
    } while (!line.empty());
  }
  /** Write out to JSON

      @param outname Output file name to write to 
   */
  void WriteJSON(const char* outname)
  {
    std::ofstream out(outname);
    out << "[\n";
    size_t ie = 0;
    for (auto iev : fEventMap) {
      auto& boards = iev.second;
      out << "  {\n"
	  << "    \"event\": " << iev.first << ",\n"
	  << "    \"time\": ["
	  << boards[0].ts << ","
	  << boards[1].ts << ","
	  << boards[2].ts << "],\n"
	  << "    \"done\": ["
	  << boards[0].done  << ","
	  << boards[1].done  << ","
	  << boards[2].done  << "],\n"
	  << "    \"adcs\": {\n";
      size_t ib = 0;
      for (auto b : boards) {
	for (size_t i = 0; i < 64; i++) {
	  unsigned short bc = BoardChannel(b.no, i);
	  unsigned int   cp = fMapping[bc];
	  out << "      " << std::setfill('0') << std::hex
	      << "\"0x" << std::setw(8) << cp << "\": "
	      << std::dec << std::setfill(' ')
	      << std::setw(6) << b.adc[i]
	      << (ib == 2 and i == 63 ? "" : ",")
	      << "\n";
	}
	ib++;
      }
      if (ie % 10000 == 0) std::cout << "Event " << ie << "\t"
				     << iev.first << std::endl;
      ie++;
      out << "    }\n"
	  << "  }"
	  << (ie == fEventMap.size() ? "" : ",")
	  << std::endl;
    }
    out.close();
  }
  /** Write the data to TTree in the a ROOT file

      @param filename File name of ROOT file to write tree to.
   */
  void WriteTree(const char* filename)
  {
    TFile*         file   = TFile::Open(filename, "RECREATE");
    TTree*         tree   = new TTree("TT","Digits");
    TClonesArray*  digits = new TClonesArray("FocalHDigit");
    FocalHDHeader* header = new FocalHDHeader;
    FocalH                  focalH;
    FocalHRO                focalHRO;
    std::map<unsigned int,FocalHDigit*> digitMap;
    focalHRO.Setup(&focalH);
    tree->SetDirectory(file);

    // Create the digits.  We do not clear the array.  Rather, we
    // clear the individual channels.
    unsigned int idx = 0;
    for (auto ich : focalHRO.fChannelInfo) {
      unsigned int       copy = ich.first;
      FocalHRO::Channel& ch   = ich.second;

      unsigned short mc,mr,cc,cr;
      focalH.CopyToCoord(copy,mc,mr,cc,cr);
      auto   digit = new ((*digits)[idx++]) FocalHDigit(mc,mr,cc,cr,0);
      digitMap[copy] = digit;
    }

    tree->Branch("FocalHDHeader", &header);
    tree->Branch("FocalHDigits", &digits);

    Trigger ie = 0;
    Trigger inc = 0;
    for (auto iev : fEventMap) {
      for (auto d : *digits) d->Clear();
      header->Clear();

      auto& boards = iev.second;

      double         sumT = 0;
      bool           com  = true;
      unsigned short cnt  = 0;
      unsigned int   msk  = 0;
      for (auto b : boards) {
	if (!b.done) {
	  com = false;
	}
	else {
	  sumT += b.ts;
	  cnt++;
	  msk  |= (1 << b.no);
	}

	for (size_t i = 0; i < 64; i++) {
	  unsigned short bc = BoardChannel(b.no, i);
	  unsigned int   cp = fMapping[bc];
	  digitMap[cp]->SetCount(b.adc[i]);
	  header->fSum += b.adc[i];
	}
      }
      double avgT = sumT / cnt;
      double varT = 0;
      for (auto b : boards) {	
	if (!b.done) continue;
	varT += b.ts * b.ts - avgT * avgT;
	cnt++;
      }
      if (cnt > 1) varT /= (cnt - 1);
      
      // for (auto b : boards) {
      //   if (!b.done) continue;
      //   if (b.done and std::abs(avgT - b.ts) > 20000)
      //     // throw std::runtime_error
      //      std::cout << ("Inconsistent time avg="+
      //  		    std::to_string(avgT)+
      // 		    " board "+std::to_string(b.no)+
      // 		    " t="+std::to_string(b.ts)+
      // 		    " (d="+std::to_string(std::abs(avgT-b.ts))+
      // 		    ")") << std::endl;
      // }
      
      header->fIncomplete = !com;
      header->fTime       = avgT;
      header->fTimeSD     = TMath::Sqrt(varT);
      header->fId         = iev.first;
      header->SetUniqueID(~msk & 0x7);// At most 3 boards!
      inc                 += !com ? 1 : 0;
      // if (msk != 0x7)
      // 	std::cout << "Missing: " << (~msk & 0x7) << std::endl;

      tree->Fill();
      if (ie % 10000 == 0)
	std::cout << "Entry " << ie << "\t (trigger " << iev.first
		  << ")" << std::endl;
      ie++;
    }
    std::cout << "Out of " << ie << " events "
	      << inc << " were incomplete ("
	      << 100*double(inc)/ie << "%)" << std::endl;
    (new TParameter<int>("energy",fEnergy))->Write();
    file->Write();
    file->Close();
  }
  /** Check the mapping from board,channel to detector coordinates

      @return true on success.
  */
  Bool_t CheckMap()
  {
    bool   ok = true;
    size_t n  = fMapping.size();
    size_t e  = 64 * NBOARD;
    if (e != n) {
      std::cerr << "Mapping size is " << n << " expected " << e
		<< std::endl;
      ok = false;
    }
    // Build inverse map and check size
    using InvMap = std::map<unsigned int,unsigned short>;
    InvMap invMap;
    for (auto bc : fMapping) {
      unsigned short boardChannel  = bc.first;
      unsigned int   moduleChannel = bc.second;
      invMap[moduleChannel]        = boardChannel;
    }
    n = invMap.size();
    if (e != n) {
      std::cerr << "Inverse mapping size is " << n << " expected " << e
		<< std::endl;
      ok = false;
    }
    std::cerr << std::hex << std::setfill('0');
    for (auto bc : fMapping) {
      unsigned short boardChannel  = bc.first;
      unsigned int   moduleChannel = bc.second;
      unsigned short test          = invMap[moduleChannel];
      if (test != boardChannel) {
	std::cerr << "0x" << std::setw(6) << boardChannel << " -> "
		  << "0x" << std::setw(8) << moduleChannel << " -> "
		  << "0x" << std::setw(6) << test << " not right"
		  << std::endl;
	ok = false;
      }
    }
    std::cerr << std::dec << std::setfill(' ');

    if (ok)
      std::cout << "Mapping is OK" << std::endl;
    return ok;
  }

  /** Input file */
  std::ifstream      fIn;
  /** Current line */
  std::string        fLine;
  /** Current line number */
  unsigned long long fLineNo;
  /** Last trigger number seen */
  Trigger            fLast;
  /** Map board/channel to detector coordinates */
  BoardMap&          fMapping;
  /** Energy - probably deduced from input file name */
  unsigned int       fEnergy;
  /** Map of events to board information.  We add board data to this
      map as we go, which means it will grow in memory.  Not a problem
      if we there are not too many events in the input. */
  EventMap fEventMap;
};

#endif
//
// EOF
//

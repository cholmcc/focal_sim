# Build

These are needed to run the simulation program:

 * GEANT4
 * ROOT6

It's important that both have been compiled with the same compiler
version, otherwise there will be a linking problem when the program
tries to create the dictionary for the custom datatype in which some
of the data is saved.

To build the program do the following:

    mkdir -p build 
    cd build 
    cmake -DGeant4_DIR=geant4-prefix ../source 
    make 
    
# GEANT4 Version

- We are currently using GEANT4 version 10.7.3 to perform the FoCal
  Standalone simulation, and we should be consistent with the GEANT4
  version we use for the test beam simulations.
- However, our code is now compatible with both versions 10
  and 11. This is automatically detected. 


# Running & modifying the simulation

Running the simulation is done by running

    cd build
    ./focal 
    
By default, this will generate a single $\pi^+$ at 80 GeV.  To run
different simulations, copy the file `../single.mac` and edit
accordingly.  E.g., to make 10 protons at 1 TeV, make a file, say
`protons.mac` with the content

    /gun/particle proton 
    /gun/energy 1 TeV 
    /run/beamOn 10
    
and then do 

    ./focal -b protons.mac 
    
The application `focal` has a number of arguments.  Do 

    ./focal -h 
    
for a list.  The script `single.sh` can also be used to accomplish the
same - e.g., 

    ../single.sh -p proton -e 1000 -n 10 

This script will automatically generate random seeds and output file
name. 
    
# Changing geometry

There are lots of variables in `source/include/constants.hh` which
change the simulation setup. These might be the most relevant ones:

 * `UseFoCalE`: swith FoCal-E on/off
 * `UseFoCalH`: swith FoCal-H on/off
 
## FoCal-E variation 

 * `UseModifiedSetup`
    * if `true` 2021 test beam setup (set `NumberPAD=1` and
      `NumberPIX=2`, `NumberW` is the number of Wolfram plates in
      front of pad and pixels)
    * if `false` use default setup (set `NumberOfLayers` so that it is
      sum of the pad and pixel layers, also set `LayerLayout`
      correctly) 

## Rotate FoCal-H

FoCal-H can be rotated by changing the variable 

    constexpr G4double HCAL_Rot_Y
    
in `source/include/constants.hh`:

- In the September 2022 test beams at SPS and PS, FoCal-H was rotated
  1 degree
- In the November 2022 test beam at SPS, FoCal-H was rotated 2.085
  degrees


# Visualisation of the setup

Start the simulation in interactive mode 

    ./focal -i 
    
The user interface and visualisation types can be set by options `-U`
and `-V` respectively. 

# Analysing the output

Please check the file [`Response.md`](Response.md) to understand what
is stored in the output. 

The ROOT script [`scripts/ReadSim.C`](scripts/ReadSim.C) contains the
class `SimReader` which allows one to read the output of the
simulation and process it in any way one would like.

The ROOT script [`scripts/ReadHCal.C`](scripts/ReadHCal.C) is an
example which determines the probability distribution of the
single-straw number of photons in FoCal-H. 

# Other notes

The code is modified from **FoCal-Standalone** by
[novitzky](https://github.com/novitzky/FoCalStandalone), and
[hrytkone](https://github.com/hrytkone/FoCalStandalone). The FoCal-H
part is from
[fun4all](https://github.com/nschmidtALICE/fun4all_eicdetectors/tree/focal_TB/simulation/g4simulation/g4focal).


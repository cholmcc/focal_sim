//____________________________________________________________________ 
//  
//  FoCAL Geant4 Simulation
//  Copyright (C) 2023 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    OnEvent.cc
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon 06 Mar 2023 09:25:16 PM CET
    @brief   Handle a new event 
 */
#include "OnEvent.hh"
#include "RootIO.hh"
#include <G4PrimaryParticle.hh>
#include <G4PrimaryVertex.hh>
#include <G4Event.hh>
#include <iostream>

//--------------------------------------------------------------------
void
OnEvent::BeginOfEventAction(const G4Event* event)
{
  G4cout << "Event # " << event->GetEventID() << "..." << std::flush;
  RootIO::Instance()->Clear();
}

//--------------------------------------------------------------------
void
OnEvent::EndOfEventAction(const G4Event* event)
{
  G4cout << "done, storing ... " << std::flush;

  G4PrimaryVertex*   ip = event->GetPrimaryVertex();
  G4PrimaryParticle* bm = ip   ->GetPrimary();
  RootIO::Instance()->SetPrimary(bm->GetPDGcode(),
				 bm->GetPx(),
				 bm->GetPy(),         
				 bm->GetPz(),         
				 bm->GetTotalEnergy(),
				 ip->GetX0(),         
				 ip->GetY0(),         
				 ip->GetZ0(),         
				 ip->GetT0());

  RootIO::Instance()->Fill();
  G4cout << "done" << G4endl;
}
//--------------------------------------------------------------------
//
// EOF
//

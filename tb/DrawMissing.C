void DrawMissing(const char* filename)
{
  TFile* file = TFile::Open(filename, "READ");
  TTree* tree = static_cast<TTree*>(file->Get("TT"));

  TH2*   hist = new TH2D("hist","", 2, -.5, 1.5, 8, -.5, 7.5);
  hist->SetYTitle("Missing boards");
  hist->SetXTitle("Event status");
  hist->SetZTitle("Frequency (%)");
  hist->GetYaxis()->SetBinLabel(1,"None");
  hist->GetYaxis()->SetBinLabel(2,"0");
  hist->GetYaxis()->SetBinLabel(3,"1");
  hist->GetYaxis()->SetBinLabel(4,"0&1");
  hist->GetYaxis()->SetBinLabel(5,"2");
  hist->GetYaxis()->SetBinLabel(6,"0&2");
  hist->GetYaxis()->SetBinLabel(7,"1&2");
  hist->GetYaxis()->SetBinLabel(8,"0&1&2");
  hist->GetXaxis()->SetBinLabel(1,"Complete");
  hist->GetXaxis()->SetBinLabel(2,"Incomplete");
  hist->SetMarkerSize(2);

  gStyle->SetOptStat(0);
  gStyle->SetPaintTextFormat("5.2f%%");
  
  TCanvas* c = new TCanvas("c","c",800*TMath::Sqrt(2),800);
  c->SetRightMargin(0.12);
  c->SetTopMargin(0.02);
  c->Divide(2,1);
  TVirtualPad* p = c->cd(1);
  p->SetLogz();
  p->SetTopMargin(0.05);
  p->SetRightMargin(0.15);
  p->SetLeftMargin(0.15);
  
  tree->Draw("FocalHDHeader.fUniqueID:FocalHDHeader.fIncomplete>>hist","",
	     "colz text");

  hist->Scale(100. / hist->GetEntries());


  Int_t     n  = tree->GetSelectedRows();
  Double_t* v1 = tree->GetV1();
  Double_t* v2 = tree->GetV2();

  TH2* corr = new TH2D("corr","",4,-1.5,2.5,4,-1.5,2.5);
  corr->SetXTitle("1^{st} Missing board");
  corr->SetYTitle("2^{nd} Missing board");
  corr->SetZTitle("Frequency (%)");
  corr->GetYaxis()->SetBinLabel(1,"None");
  corr->GetYaxis()->SetBinLabel(2,"0");
  corr->GetYaxis()->SetBinLabel(3,"1");
  corr->GetYaxis()->SetBinLabel(4,"2");
  corr->GetXaxis()->SetBinLabel(1,"None");
  corr->GetXaxis()->SetBinLabel(2,"0");
  corr->GetXaxis()->SetBinLabel(3,"1");
  corr->GetXaxis()->SetBinLabel(4,"2");
  corr->SetMarkerSize(2);
  for (Int_t i = 0; i < n; i++) {
    UInt_t miss = v1[i];
    UInt_t stat = v2[i];
    if (stat == 0) corr->Fill(-1.,-1.);
    else {
      UInt_t b0 = (miss & 0x1);
      UInt_t b1 = (miss & 0x2);
      UInt_t b2 = (miss & 0x4);
      if (b0>0 and b1>0) corr->Fill(0.,1.);
      if (b0>0 and b2>0) corr->Fill(0.,2.);
      if (b1>0 and b2>0) corr->Fill(1.,2.);
      if (b0>0 and b1==0 and b2==0) corr->Fill(0.,0.);
      if (b1>1 and b0==0 and b2==0) corr->Fill(1.,1.);
      if (b2>1 and b0==0 and b1==0) corr->Fill(2.,2.);
    }
  }
  corr->Scale(100./n);
  p = c->cd(2);
  p->SetLogz();
  p->SetTopMargin(0.05);
  p->SetLeftMargin(0.15);
  p->SetRightMargin(0.15);
  corr->Draw("colz text");
  
  c->Modified();
  c->Update();
  c->cd();
  
}



    
  

# Simulated response 

Here, I'll go through the simulated detector response, with particular
emphasis on the FoCal-H part. 

Yours,

_Christian_

## The output 

The output is a ROOT file (by default `hits.root`) with a
[`TTree`](https://root.cern/doc/master/classTTree.html) in it.  That
tree is called `DataTree`. 

    ```C++
    TFile* file = TFile::Open("hits.root","READ");
    TTree* tree = static_cast<TTree*>(file->Get("DataTree"));
    ```
    
The `TTree` contains an entry for every primary particle - i.e., one
for each particle produced by the particle gun.  

    ```C++
    Int_t nEvents = tree->GetEntries();
    ```
    
There are a number of _global_ branches that record information about
the incident particle.

- `event` The event number 
- `particle_px` Particle momentum along $x$ - $p_x$ - in MeV 
- `particle_py` Particle momentum along $y$ - $p_y$ - in MeV 
- `particle_pz` Particle momentum along $z$ - $p_z$ - in MeV 
- `particle_en` Particle energy - $\sqrt{m^2+p_x^2+p_y^2+p_z^2}$ - in MeV
- `vertex_x` Production vertex in $x$ - $v_x$ in mm
- `vertex_y` Production vertex in $y$ - $v_y$ in mm
- `vertex_z` Production vertex in $z$ - $v_z$ in mm


```C++
Int_t event;
Float_t px, py, pz, en;
Float_t vx, vy, vz;

std::map<std::string,void*> ba{{"event",      &event},
                               {"particle_px",&px},
                               {"particle_py",&py},
                               {"particle_pz",&pz},
                               {"particle_en",&en},
                               {"vertex_x",   &vx},
                               {"vertex_y",   &vy},
                               {"vertex_z",   &vz}};
for (auto b : ba) 
  tree->SetBranchAddress(b.first.c_str(),b.second);
```

### FoCal-H information 

There are a number of branches in the `TTree` for FoCal-H, more
specifically one for each module.

- `HCAL_ModuleX<x>_ModuleY<y>` where $x$ and $y$ are the module
  column and row, respectively. Both of these numbers range from 0
  to 2, inclusive.
  
Each of these branches contain a `std::vector` of
`std::pair<int,float>`.  These `std::vector` objects are of fixed
size, corresponding to the number of rows and columns within each
module 

$N_{\mathrm{straws}}=N_x \times N_y\quad,$

where $N_x$ and $N_y$ is the number of columns and rows, respectively,
within each module. 

```C++
gROOT->GetInterpreter()
  ->GenerateDictionary("vector<pair<int,float>>", "vector");
  
using HCalModule  = std::vector<std::pair<int,float>>;
using HCalModules = std::array<std::array<HCalModule*,3>,3>;

HCalModules hcalModules;

int x = 0;
for (auto& col : hcalModules) {
  int y = 0;
  for (auto& cell : col) {
    cell = new HCalModule();
    tree->SetBranchAddress(Form("HCAL_ModuleX%d_ModuleY%d",x,y++), &cell);
  }
  x++;
}
```

Thus for each module, we have 

- `first` (`int`) is the "tower" - or straw - number in the module. 
- `second` (`float`) is the response of the detector summed over all
  steps that deposited energy in a straw.
  
More about the content of these two entries below.  

Note that each entry in the `std::vector` corresponds to the _sum_ of
all steps in the simulation.  That is, if the simulation detects that
something happened in straw $s$, then the user stepping routine
([`SteppingAction.cc`](sources/src/SteppingAction.cc)) calculates the
response $\Delta_i$ and the response pair $(s,\Delta)$ is updated.
Thus, the member `second` contains the sum of the detector response to
each "event" in the simulation that deposited energy in that straw

$$\Delta_s = \sum_{i\in s} \Delta_i\quad.$$


## FoCal-E information

There are two kinds of responses - one for the pads and one for the
pixels. 

### Pads 

There is a single branch that contains the information from the pads. 

- `data_pad` which is a fixed sized array for floating point values.
  The size of the array is given by 
  
  $N_{\mathrm{ch}}=N_{x}\times N_{y} \times N_{\mathrm{module}}\quad,$ 
  
  where $N_x$ and $N_y$ are the number of pads in the $x$ and $y$
  directions, respectively, within each pad module, and
  $N_{\mathrm{module}}$ is the number of pad modules 
  
  ```C++
  constexpr Int_t padNx  = 9;
  constexpr Int_t padNy  = 8;
  constexpr Int_t padNm  = 18;
  constexpr Int_t padNch = padNx * padNy * padNm;

  std::array<float,padNch> ecalPads;
  tree->SetBranchAddress("data_pad", &ecalPads[0]);
  ```
  
  The data corresponding to module $m$, column $c$, row $r$ is then
  indexed as 
  
  $$q = m \times (r \times N_x + c) + C\quad.$$
  
  where $C=1$. 
  
  The data stored in each entry in this array is the sum energy loss 
  
  $$\Delta_q = \sum_{i\in q}\Delta_i\quad.$$
  
  where $\Delta_i$ is the energy loss in a single step in a given pad. 
  
### Pixels 

There are several branches for the pixel modules.  

- `data_pix<m>_alpide<y>` where $m$ is the module number, and $y$ is
  the row (column?) of the Alpide sensor and chip. 
  
  Each of these branches are again `std::vector` of
  `std::pair<int,float>`.  However, the `std::vector` objects are of
  fixed size given by 
  
  $$N_{\mathrm{ch/alpide}} = N_x \times N_y\quad,$$
  
  where $N_x=1024$ and $N_y=512$ is the number columns and rows,
  respectively, within each Alpide sensor and chip. 
  
  The number of Alpide sensors and chips is then 
  
  $$N_{\mathrm{alpide}} = N_{\mathrm{layer}}\times N_c \times
  N_r\quad,$$ 
  
  where $N_{\mathrm{layer}}=2$, $N_r=3$, and $N_c=3$, are the number
  of layers of Alpides, number of rows and columns, respectively,
  within each module. 
  
  ```C++
  using PxlModule = std::vector<std::pair<int,float>>;
  
  constexpr Int_t pxlNl = 2;
  constexpr Int_t pxlNc = 3;
  constexpr Int_t pxlNr = 3;
  constexpr Int_t pxlNa = pxlNl * pxlNc * pxlNr;
  constexpr Int_t pxlNm = 2;
        
  using PxlModules = std::array<std::array<PxlModule*,pxlNa>,pxlNm>;
  PxlModules pxlModules;
        
  int m = 0;
  for (auto& mod : pxlModules) {
    int c = 0;
    for (auto& chp : mod) {
      chp = new PxlModule();
      tree->SetBranchAddress(Form("data_pix%d_alpide%d",m,c++),&chp);
    }
    m++;
  }
  ```
  
As for HCal, each entry in the branches is a pair 

- `first` (`int`) is the pixel number within a give Alpide chip and
  sensor. 
- `second` (`float`) is the response of the detector summed over all
  steps that deposited energy in a pixel.
  
  
Note that each entry in the `std::vector` corresponds to the _sum_ of
all steps in the simulation.  That is, if the simulation detects that
something happened in pixel $p$, then the user stepping routine
([`SteppingAction.cc`](sources/src/SteppingAction.cc)) calculates the
response $\Delta_i$ and the response pair $(p,\Delta)$ is updated.
Thus, the member `second` contains the sum of the detector response to
each "event" in the simulation that deposited energy in that pixel

$$\Delta_p = \sum_{i\in p} \Delta_i\quad.$$
  
  
### General remarks 

For all detector channels, the stored information is the summed
response 

$$\Delta_x = \sum_{i \in x}\Delta_i\quad.$$

For the FoCal-E thin absorbers, this makes sense.  However, for
FoCal-H, it means that we cannot query the output valuable information
such as the shower profile.  

Furthermore, we do not have any information about the particle that
produced a particular response.  This precludes us from asking
questions about which type of particles we are dealing with, and in
particular which kinds of interactions they had along their
trajectory. 

While the [`focal_sim`](https://gitlab.com/cholmcc/focal_sim) package
may store an excessive amount of information, it does give us the
possibility to ask all sorts of questions, including questions we
didn't think of from the get-go. 

### Looping over the information

With code presented above, we can loop over the data 

```C++
for (Int_t entry = 0; entry < nEvents; entry++) {
  tree->GetEntry(entry);

  for (auto col : hcalModules) 
    for (auto cell : col) 
      std::cout << "HCal straws " << cell->size() << std::endl;
   
  for (auto pad : ecalPads)
    if (pad > 0)
      std::cout << "ECal pad    " << pad << std::endl;
  
  for (auto mod : pxlModules) 
    for (auto chp : mod)
      std::cout << "ECal pixels " << chp->size() << std::endl;
}
```

## Calculated response

If the simulation determines that something happened in a _sensitive_
volume i.e., a volume which corresponds to a sensor from which we
expect a signal, the user supplied code is called.  That code is in
the class [`SteppingAction`](source/src/SteppingAction.cc), and in
particular in the member function
`SteppingAction::UserSteppingAction`. 

A number of common calculations, is done irrespective of whether the
simulation "event" or _step_ - happened in the FoCal-E pads or pixels,
or in the FoCal-H straws.

```C++
auto  preStepPoint = step->GetPreStepPoint();
auto  theTouchable = preStepPoint->GetTouchableHandle();
auto  volume       = theTouchable->GetVolume()->GetLogicalVolume();
auto  volumeName   = theTouchable->GetVolume()->GetName();
Int_t copyNo       = theTouchable->GetCopyNumber();
auto  pos          = step->GetPreStepPoint()->GetPosition();
```

Above, `step` is a structure that records what happened in this
step. From this we can get the volume (`volume`) and name of that
volume (`volumeName`), as well as the copy number, $i$, of that volume
(`copyNo`), and the position (in global coordinates, `pos`), where the
step took place.

A few words about the copy number and volumes.  A volume has a shape
and some material, or may itself be built up of one or more of such
volumes (then, it is called an _assembly_).  A volume, however,
occupies no space in the simulation.  We must therefore _place_ a
_copy_ of a volume, and thus make that copy concrete within the
simulation.   The _copy number_ is simple the serial number of the
copy of a given volume.  As such, it is _unique_ to that copy and is
associated with a particular _place_ in the 3D space of the
simulation. 

```C++
auto edepStep = step->GetTotalEnergyDeposit();
auto eionStep = (step->GetTotalEnergyDeposit() 
                 - step->GetNonIonizingEnergyDeposit());
Double_t lightYield = 0;
```

Next, we get the total energy deposited $\Delta_{\mathrm{tot}}$
(`edepStep`) in this step, as well as the energy deposited solely due
to ionisation $\Delta_{\mathrm{ion}}$ (`eionStep`).

Note that the non-ionisation energy loss, AFAIR, is generally small
and most often $\Delta_{\mathrm{ion}}\approx\Delta_{\mathrm{tot}}$. 

In the code, we only go on if $\Delta_{\mathrm{tot}}>0$.  What happens
next, depends which sensitive volume the step happened. 

### ECal pads 

```C++
fEventAction->AddeDepPAD(copyNo-IDnumber_PAD_First, edepStep); 
```

The energy loss $\Delta_{\mathrm{tot}}$ is simply added to the sum of
the energy loss in a given pad identified by the copy number $i$
(`copyNo`). 

### ECal pixels 

```C++
auto  local_pos = theTouchable->GetHistory()
                  ->GetTopTransform().TransformPoint(pos);
G4int ix      = (local_pos.x() + PIX_Alpide_X/2.)/(PIX_Alpide_X/NpixX);
G4int iy      = (local_pos.y() + PIX_Alpide_Y/2.)/(PIX_Alpide_Y/NpixY);
G4int ialpide = copyNo-IDnumber_PIX_First;
G4int id      = ix + NpixX*iy;
fEventAction->AddeDepPIX(ialpide, id, edepStep);
```

After some calculations to identify the specific pixel module and
Alpide sensor and chip, sum of energy loss in a particular pixel
(`ix`,`iy`) in the given Alpide (`ialpide`) is updated with the total
energy loss  $\Delta_{\mathrm{tot}}$. 

### HCal 

```C++
copyNo        -= IDnumber_SCINT_First;
G4int iModule =  copyNo / (NtowerX*NtowerY) ;
copyNo        -= iModule*(NtowerX*NtowerY) ;
G4int iTower  =  copyNo;
G4int iCapX   =  copyNo/NtowerX;
copyNo        -= iCapX*NtowerX;
G4int iCapY   =  NtowerY;
```

Based on the copy number $i$, we calculate the module column and row
coordinates (`iCapX` and `iCapY`, respectively). 

The next steps is where things get different. 

```C++
if (light_scint_model) {
  auto emSaturation = G4LossTableManager::Instance()->EmSaturation();
  lightYield = emSaturation->VisibleEnergyDepositionAtAStep(step);
} else {
  lightYield = eionStep;
}
```

If we have enabled the use of a light yield model (`EmSaturation`),
then we calculate the photon energy deposited, $\Delta_{\gamma}$,
corresponding to this step.  The calculations done by this model will
not be covered here, but suffice to say that it is calculated from the
total energy deposited $\Delta_{\mathrm{tot}}$.

If we do _not_ wish to use that model, then we set 

$$\Delta_\gamma = \Delta_{\mathrm{ion}}\quad,$$

and will use that in the rest of the calculations for the response of
FoCal-H. 

```C++
lightYield *= 8000 / MeV;
```

For BFC12, the polystyrene in the scintilators, the number of photons
$N_{\gamma}$ produced is given by 

$$N_{\gamma}(\Delta_{\gamma}) = 8\times10^{6}\, \mathrm{GeV}^{-1}
\Delta_{\gamma}\quad.$$ 

Next, we account for the efficiency of capturing light by the
scintillating material.  There's a number of figures floating around
for the efficiency $\varepsilon$ 

| Material                          | Efficiency |
|-----------------------------------|------------|
| Kuraray multi-clad round fibre(*) | 5.4%       |
| BFC12 multi-clad                  | 5.6%       |
| BFC12 single-clad                 | 3.4%       |

[(*)](https://www.kuraray.com/uploads/5a717515df6f5/PR0150_psf01.pdf)

The current choice is to use an estimate of $\varepsilon=5\%$.
However, since the HCal fibres are not coated with aluminium in both
ends, the efficiency is halved, so that 

$$\varepsilon = 2.5\%\quad.$$

```C++
lightYield *= 0.025;
```

Now, since the light travelling in the scintilators will attenuate
(die out), we determine the distance from the current step to the of
the to the Photo-multipliers (PMs - probably should be to the end of
the scintilator).

```C++
G4double zPosSiPM = (1214 + 50)*mm; 
G4double dSiPM    = zPosSiPM - step->GetPostStepPoint()
                    ->GetPosition().z();
```

Some comments: 

- The attenuation happens in the fibre, not all the way to the PMs.
  The distance should be calculated to the end of the fibre. 
- The $z$ coordinate of the PMs is hard-coded, and _does not_ take
  into account any rotation of FoCal-H.  Instead, the code should
  query the geometry for the end of the attenuation and calculate the
  distance via 3-vector coordinates.  That is, if the step is at
  $\vec{p}$ and the end of the attenuation happens at $\vec{q}$, then 
  
  $$d = |\vec{q}-\vec{p}|\quad.$$
  
Now that we have the distance $d$, we calculate the attenuation as an
exponential decay

$$a = e^{-d/L}\quad,$$ 

where $L=2.7\,\mathrm{m}$ is the attenuation length stated in the
Saint Gobain datasheet for BCF12. 

```C++
G4double fiberAttLength = 2.7*m;
lightYield *= exp(-dSiPM/fiberAttLength);
```

Finally, we can update the summed response in the given fibre 

```C++
fEventAction->AddeDepSCINT(iModule, iTower, lightYield); 
```

#### Some remarks 

The above additional steps (conversion to $N_{\gamma}$, efficiency,
and attenuation), are much better dealt with _post_ simulation.  Of
course, that requires that enough information is stored to do these
calculations in a later step.  What we need 

- Single step energy loss $\Delta_{\mathrm{tot}}$ 
- Location of the step $\vec{p}$

One reason why it is better to do this in a _post_ simulation
analysis, is that we do not need to redo the simulation (which is
costly) every time we want to change a parameter. 

Also note, that the above does not distinguish between whether the
energy deposition is the modelled photon energy ($\Delta_\gamma$) or
the ionisation energy ($\Delta_{\mathrm{ion}}$), and the various
effects (conversion to $N_{\gamma}$, efficiency, and attenuation) may
not be appropriate if we store $\Delta_{\mathrm{ion}}$. 

Finally, attenuation should probably be modelled as a stochastic
process.  That is, some randomness should be determine the exact
attenuation factor $a$.  Also note that $a>60\%$ in the range we are
dealing with. 

The [`focal_sim`](https://gitlab.com/cholmcc/focal_sim) package _does
not_ directly implement the additional effects  (conversion to
$N_{\gamma}$, efficiency, and attenuation) but does store
$\Delta_{\gamma}$ and $\Delta_{tot}$ so these effects can be
implemented in a _post_ simulation step.  The main difficulty lies in
determining the distance $d$ to the end of the attenuation, but it can
be done using the geometry written to disk (in `geometry.root`).  The
rest is simple multiplication. 


        
<!--
pandoc -V geometry=a4paper,margin=2cm -o Response.pdf  Response.md 
-->

# Attenuation of number of photons

Here's what I did 


    cd focal_sim 
    cd old/build 
    cmake ../sources
    make
    ../single.sh -n 10 
    # Move hits_pi+_80_....root to hits.root 
    root -l ../scripts/ReadHCal.C 
    
Drawn histogram will be in the file `focalh_att.root`.  Now for my
stuff 


    cd ../../g4/data
    make 
    make run NEV=10 PZ=80 
    # Output will be in 80_10_events.root
    root -l LoadData.C Attenuate.C\(-1,\"80_10_events.root\"\)
    
The drawn histogram will be in the file `80_10_att.root` 

## Plots

### Old 

![](Attenuation_old.png)

### G4 

![](Attenuation_g4.png)



<!-- 
pandoc -V geometry=a4paper,margin=2cm -o Attenuation.pdf  Attenuation.md 
-->

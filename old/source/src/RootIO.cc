//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
/// \file persistency/P01/src/RootIO.cc
/// \brief Implementation of the RootIO class
//
//
//--------------------------------------------------------------------
#include <sstream>
#include <vector>

#include "RootIO.hh"
//
#include "G4SDManager.hh"
#include "G4HCofThisEvent.hh"
#include "G4EventManager.hh"
#include "G4Event.hh"

//--------------------------------------------------------------------
static RootIO* instance = 0;

//--------------------------------------------------------------------
bool RootIO::Open(const std::string& filename) 
{
  // initialize ROOT
  gSystem->Load("libFoCal_MainClassesDict");
  gInterpreter->GenerateDictionary("vector<pair<int,float>>", "vector");

  //gDebug = 1;
  TFile* file = new TFile(filename.c_str(),"RECREATE");
  fHitTree    = new TTree("DataTree", "Data", 0);
  fHitTree->SetDirectory(file);
  fHitTree->Branch("event", &event, "event/I");

  char helper[4000];
  sprintf(helper, "data_pad[%d]/F", NpadX*NpadY*NumberPAD);
  fHitTree->Branch("data_pad", &data_pad, helper );

  for (int ipix=0; ipix<NumberPIX; ipix++) {
    for (int ialpide=0; ialpide<NalpideLayer*NumberPixRow*NumberPixCol; ialpide++) {
      fHitTree->Branch(Form("data_pix%d_alpide%d", ipix, ialpide),
                       &data_alpide[ipix*NalpideLayer*NumberPixRow*NumberPixCol + ialpide]);
    }
  }

  for (int ihcalx = 0; ihcalx < N_HCAL_Module_X; ihcalx++) {
    for (int ihcaly = 0; ihcaly < N_HCAL_Module_Y; ihcaly++) {
      fHitTree->Branch(Form("HCAL_ModuleX%d_ModuleY%d", ihcalx, ihcaly),
                       &data_scint[ihcalx*N_HCAL_Module_Y + ihcaly]);
    }
  }  // fHitTree->Branch("data_scint", &data_scint); 

  fHitTree->Branch("particle_px", &particle_px, "particle_px/F");
  fHitTree->Branch("particle_py", &particle_py, "particle_py/F");
  fHitTree->Branch("particle_pz", &particle_pz, "particle_pz/F");
  fHitTree->Branch("particle_en", &particle_en, "particle_en/F");

  fHitTree->Branch("vertex_x", &vertex_x, "vertex_x/F");
  fHitTree->Branch("vertex_y", &vertex_y, "vertex_y/F");
  fHitTree->Branch("vertex_z", &vertex_z, "vertex_z/F");

  return true;
}

//--------------------------------------------------------------------
RootIO* RootIO::GetInstance()
{
  if (instance == 0) 
    instance = new RootIO();
  return instance;
}

//--------------------------------------------------------------------
void RootIO::WriteEvent(int e)
{
  event = e;
}

//--------------------------------------------------------------------
void RootIO::WritePad(int i, float a)
{
  data_pad[i] = a;
}

//--------------------------------------------------------------------
void RootIO::WriteAlpide(int ialpide, int i, float a)
{
  data_alpide[ialpide].push_back(std::make_pair(i, a));
}

//--------------------------------------------------------------------
void RootIO::WriteScint(int iModule, int itower, float a) 
{
  data_scint[iModule].push_back(std::make_pair(itower, a));
}

//--------------------------------------------------------------------
void RootIO::WriteVertex(float x, float y, float z)
{
  vertex_x = x;
  vertex_y = y;
  vertex_z = z;
}

//--------------------------------------------------------------------
void RootIO::WriteParticleKinematics(float px, float py, float pz, float en)
{
  particle_px = px;
  particle_py = py;
  particle_pz = pz;
  particle_en = en;
}

//--------------------------------------------------------------------
void RootIO::Clear(){
  for (int i = 0; i < NpadX*NpadY*NumberPAD; i++) data_pad[i] = 0;
  for (int i = 0; i < NalpideLayer*NumberPixCol*NumberPixRow*NumberPIX; i++) 
    data_alpide[i].clear();

  for (int i = 0; i < N_HCAL_Module_X*N_HCAL_Module_Y; i++) 
    data_scint[i].clear();

  particle_px = 0;
  particle_py = 0;
  particle_pz = 0;
  particle_en = 0;
  vertex_x = 0;
  vertex_y = 0;
  vertex_z = 0;
}

//--------------------------------------------------------------------
void RootIO::Fill()
{
  if (not fHitTree) 
    throw std::runtime_error("No TTree defined");
  
  fHitTree->Fill();
}

//--------------------------------------------------------------------
void RootIO::Close()
{
  if (fHitTree) {
    auto file = fHitTree->GetCurrentFile();
    if (file) {
      file->Write("", TObject::kOverwrite);
      file->Close();
    }
  }
  fHitTree = nullptr;
}

//--------------------------------------------------------------------
//
// EOF
//

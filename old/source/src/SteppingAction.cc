#include "SteppingAction.hh"
#include "EventAction.hh"
#include "Geometry.hh"

#include "G4Step.hh"
#include "G4Event.hh"
#include "G4RunManager.hh"
#include "G4LogicalVolume.hh"
#include "G4ParticleDefinition.hh"
#include "G4EmSaturation.hh"
#include "G4LossTableManager.hh"
#include "G4OpticalPhoton.hh"
#include "Analysis.hh"

G4int I=0;
G4double NTRACK[10];
G4double COPYNUM[10];

SteppingAction::SteppingAction(EventAction* eventAction)
  : G4UserSteppingAction(),
    fEventAction(eventAction),
    light_scint_model(1),
    fScoringVol_PAD(0),
    fScoringVol_PIX(0),
    fScoringVol_SCINT(0)
{}


SteppingAction::~SteppingAction()
{}


void SteppingAction::UserSteppingAction(const G4Step* step)
{
  if (UseFoCalH && ( !fScoringVol_PAD || !fScoringVol_PIX || !fScoringVol_SCINT ) ) {
    const Geometry* geometry
      = static_cast<const Geometry*>
      (G4RunManager::GetRunManager()->GetUserDetectorConstruction());

    fScoringVol_PAD   = geometry->GetScoringVol_PAD();
    fScoringVol_PIX   = geometry->GetScoringVol_PIX();
    fScoringVol_SCINT = geometry->GetScoringVol_SCINT();
    

  }
  else if (!UseFoCalH && (!fScoringVol_PAD || !fScoringVol_PIX) ) {
    const Geometry* geometry
      = static_cast<const Geometry*>
      (G4RunManager::GetRunManager()->GetUserDetectorConstruction());

    fScoringVol_PAD   = geometry->GetScoringVol_PAD();
    fScoringVol_PIX   = geometry->GetScoringVol_PIX();
    fScoringVol_SCINT = geometry->GetScoringVol_SCINT();
    

  }
  // Get PreStepPoint and TouchableHandle objects
  auto  preStepPoint = step->GetPreStepPoint();
  auto  theTouchable = preStepPoint->GetTouchableHandle();
  auto  volume       = theTouchable->GetVolume()->GetLogicalVolume();
  auto  volumeName   = theTouchable->GetVolume()->GetName();
  G4int copyNo       = theTouchable->GetCopyNumber();
  auto  pos          = step->GetPreStepPoint()->GetPosition();

  if (volume != fScoringVol_PAD
      && volume != fScoringVol_PIX
      && volume != fScoringVol_SCINT) return;

  auto edepStep = step->GetTotalEnergyDeposit();
  auto eionStep = (step->GetTotalEnergyDeposit() -
                   step->GetNonIonizingEnergyDeposit());
  G4double lightYield = 0;

  if (edepStep > 0){
    if ( volume == fScoringVol_PAD ) {
      fEventAction->AddeDepPAD(copyNo-IDnumber_PAD_First, edepStep);
    } else if ( volume == fScoringVol_PIX ) {
      auto  local_pos = theTouchable->GetHistory()->GetTopTransform().TransformPoint(pos);
      G4int ix      = (local_pos.x() + PIX_Alpide_X/2.)/(PIX_Alpide_X/NpixX);
      G4int iy      = (local_pos.y() + PIX_Alpide_Y/2.)/(PIX_Alpide_Y/NpixY);
      G4int ialpide = copyNo-IDnumber_PIX_First;
      G4int id = ix + NpixX*iy;
      fEventAction->AddeDepPIX(ialpide, id, edepStep);
    } else if ( volume == fScoringVol_SCINT ) {
      copyNo        -= IDnumber_SCINT_First;
      G4int iModule =  copyNo / (NtowerX*NtowerY) ;
      copyNo        -= iModule*(NtowerX*NtowerY) ;
      G4int iTower  =  copyNo;
      if (light_scint_model) {
        auto emSaturation = G4LossTableManager::Instance()->EmSaturation();
        lightYield = emSaturation->VisibleEnergyDepositionAtAStep(step);
      } else {
        lightYield = eionStep;
      }


      // Real light Yield simulation
      // 
      // Polystyrene light yield: total 8000 photons/MeV for BCF12
      lightYield *= 8000 / MeV;
	    
	    
      // Light capturing efficiency due to total internal reflection in
      // the fiber
      // 
      // 5.4 % for Kuraray multiclad round fiber
      // 
      // https://www.kuraray.com/uploads/5a717515df6f5/PR0150_psf01.pdf
      //
      // 5.6 % for BCF-12 multiclad
      // 3.4 % for BCF-12 single cladding

      // TO DO: Single cladding vs multicladding?
      // BCF-12 can be both
      // 
      // Taking only one of the directions since the other fiber end
      // is not aluminized for reflection
      // 
      // Taking 5 % on average as a good approximation for the
      // trapping efficiency
      // 
      // Can be easily recovered at analysis level since it is a
      // global constant factor
      lightYield *= 0.025 ;

      //Getting the distance from the energy deposit point (i.e. the
      //light emission point) to the SiPM
      G4double zPosSiPM = (1214 + 50)*mm;
      G4double dSiPM    = zPosSiPM - step->GetPostStepPoint()->GetPosition().z(); // zSIPM

      G4double fiberAttLength = 2.7*m; //Saint Gobain datasheet for BCF-12
	    
      lightYield *= exp(-dSiPM/fiberAttLength);


	    
      // G4cout << "lightYield: " << lightYield << "  pos: " << step->GetPostStepPoint()->GetPosition().z() << "  Att:   " << exp(-dSiPM/fiberAttLength) << G4endl;
      fEventAction->AddeDepSCINT(iModule, iTower, lightYield); 
    }
  }
}

//____________________________________________________________________ 
//  
//  FoCAL Geant4 Simulation
//  Copyright (C) 2023 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    FocalHFluxer.cc
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon 06 Mar 2023 09:25:16 PM CET
    @brief   Sensitive detector to make hits in scintillating fibres
 */
#include "FocalHFluxer.hh"
#include <G4Step.hh>
#include <G4EmSaturation.hh>
#include <G4LossTableManager.hh>
#include <G4Track.hh>
#include <G4SystemOfUnits.hh>
#include <G4ParticleTypes.hh>
#include "RootIO.hh"

//____________________________________________________________________
void
FocalHFluxer::Initialize(G4HCofThisEvent *)
{
  if (!fFirst) return;

  fFirst = false;
  std::cout << "Initialize the FocalHFluxer" << std::endl;
  RootIO::Instance()->MakeFocalHFlux(isActive());
}

//____________________________________________________________________
G4bool
FocalHFluxer::ProcessHits(G4Step* step, G4TouchableHistory*)
{
  auto frontBack = IsFrontOrBack(step);
  bool front     = frontBack.first;
  bool back      = frontBack.second;
  if (!front and !back) return false;
  
  G4StepPoint*      before    = step  ->GetPreStepPoint();
  G4StepPoint*      after     = step  ->GetPreStepPoint();
  G4ThreeVector     position1 = before->GetPosition();
  G4ThreeVector     position2 = after ->GetPosition();
  G4double          time      = before->GetGlobalTime();
  G4ThreeVector     momentum  = before->GetMomentum();
  G4double          energy    = before->GetTotalEnergy();
  G4Track*          track     = step  ->GetTrack();
  G4int             pdg       = track ->GetDynamicParticle()->GetPDGcode();
  G4int             trackNo   = track ->GetTrackID();

  G4ThreeVector& pos = (front ? position1 : position2);
  // G4cout << trackNo << " Particle " << pdg << " "
  //        << (front ? "enters" : (back ? "exits" : "?"))
  //        << " the flux volume at " << pos << G4endl;
  
  RootIO::Instance()->AddFocalHFlux(trackNo,
				    front,
				    back,
				    pdg,
				    pos.x(),
				    pos.y(),
				    pos.z(),
				    time,
				    momentum.x(),
				    momentum.y(),
				    momentum.z(),
				    energy);
  return true;
}
//
// EOF
//


#____________________________________________________________________ 
#  
#  FoCAL-H Geant 4 Simulation
#  Copyright (C) 2023 Christian Holm Christensen <cholm@nbi.dk>
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public License
#  as published by the Free Software Foundation; either version 2.1
#  of the License, or (at your option) any later version.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free
#  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
#  02111-1307 USA
#
TIME		:= /usr/bin/time --verbose 
ROOT		:= root
ROOT_FLAGS	:= -l --web=off
MAIN		:= ../main
MAIN_FLAGS	:= -O -Z -t pi+ 
NEV		:= 1
PZ		:= 50
NJOBS		:= 10
NSIM		:= -2
INPUT		:= events.root
PROFILER	:= perf record --call-graph dwarf -o $(PZ)_$(NEV)_events.perf
GPROFILER	=  LD_PRELOAD=/usr/lib/x86_64-linux-gnu/libprofiler.so \
		   CPUPROFILE=$(PZ)_$(NEV)_events.prof

srcdir		:= ../
include $(srcdir)Files.mk

ifdef PROFILE
TIME		:= $(PROFILER)
endif
ifdef GPROFILE
TIME		:= $(GPROFILER)
endif

help:; @$(info $(helpmsg)) :

build:	$(MAIN)
	$(ROOT) $(ROOT_FLAGS) -b -q LoadData.C LoadAna.C

$(MAIN):$(SIM_SOURCES) $(SIM_HEADERS)
	$(MAKE) -C ../ build

interactive:$(MAIN)
	$(MAIN) -i

run:		$(PZ)_$(NEV)_events.root
analyse:	$(PZ)_$(NEV)_analysis.root

%_events.root:$(MAIN)
	$(TIME) $(MAIN) $(MAIN_FLAGS) -p $(PZ) -n $(NEV) -o $@

%_events.root:	PZ=$(word 1, $(subst _, ,$*))
%_events.root:	NEV=$(word 2, $(subst _, ,$*))

%_analysis.root:%_events.root $(ANA_SOURCES)
	$(ROOT) $(ROOT_FLAGS) $(notdir $(word 2,$^))\(-1,\"$<\"\)

%_att.root:%_events.root $(ATT_SOURCES)
	$(ROOT) $(ROOT_FLAGS) $(notdir $(word 2,$^))\(-1,\"$<\"\)

localclean:
	rm -f *~ *.log *.gdml

localrealclean:	localclean
	rm -f *.root
	rm -f *.list

clean:	localclean
	$(MAKE) -C ../ clean

realclean:localrealclean
	$(MAKE) -C ../ realclean 

production:build
	$(MAKE) $(NJOBS)_$(NEV)_$(PZ).jobs NJOBS=$(NJOBS) NEV=$(NEV) PZ=$(PZ)

%.root: %.list $(ANA_SOURCES)
	$(TIME) $(ROOT) $(ROOT_FLAGS) $(notdir $(word 2,$^))\(-1,\"$<\"\)

$(NJOBS)_$(NEV)_$(PZ).jobs:build
	@echo "Running $(NJOBS) of $(NEV) events at $(PZ)GeV"
	@for i in `seq $(NJOBS)` ; do \
	  bash -c "printf '0x%08x 0x%08x\n' \$${RANDOM} \$${RANDOM}" ; done | \
	  $(TIME) parallel -t --jl $@ -j$(NSIM) \
		$(MAIN) $(MAIN_FLAGS) -p $(PZ) -n $(NEV) -s {} -o auto \
		">" $(basename $@)_{='$$_=~tr/ /_/'=}.log "2>&1"

%.list:%.jobs
	for i in `sed -n 's/.*\($(basename $@)_[x0-9a-f_ ]*\.log\).*/\1/p' < $<` ; do \
	  sed -n 's/Output file set to "\(.*\)"/\1/p' < $$i ; \
	  done > $(basename $@).list
	@echo "Production file list in $(basename $@).list, $(MAKE) $(basename $@).root to analyse"


define helpmsg = 
- HOW TO USE MAKE WITH THIS PROJECT

    make TARGET [VAR=VALUE]*

"generates" TARGET, which must be a valid target. If no TARGET is
given, it defaults to "help".  A variable VAR can be set to a VALUE
directly on the command line.

Valid targets for this project are

- help - show this help

- build - builds the code but does nothing else

- interactive - Builds code and starts an interactive session.
  Setting can be tuned as per target events.root (see below).

- run - same as <PZ>_<NEV>_events.root.  To change the settings, set the
  variables PZ and NEV on the command line e.g.,

     make run PZ=200 NEV=100

- <PZ>_<NEV>event.root - Runs a single simulation.  By default, the simulation
  is run in batch mode, 1 event is generated with a single 50 GeV/c
  pi+.  However, this can be changed by setting the variables

  - NEV - sets number of events e.g., NEV=10
  - PZ - sets z-momentum of pion, e.g., PZ=200

  For example, to generate 54321 events with pz=123, in
  batch mode, do

      make 123_54321_events.root NEV=54321

  Other flags for the simulation can be specified in the variable MAIN_FLAGS.
  The default value is MAIN_FLAGS="$(MAIN_FLAGS)".   To turn on the FoCAL-E
  detector, do for example

      make 123_54321_events.root NEV=54321 MAIN_FLAGS="-O -t pi+"

  Try

      $(MAIN) --help

  for available options

- analyse - the same as <PZ>_<NEV>_analysis.root

- <PZ>_<NEV>_analysis.root - performs an analysis on <PZ>_<NEV>_events.root. 
  The analysis performed are
  - Radial and longitudinal profile
  - Particle species leaving the FoCAL-H volume at the back
  - Digitisation of the energy loss 

- production - same as <NJOBS>_<NEV>_<PZ>.log.  To change the NJOBS,
  NEV, and PZ parameters set these on the command line e.g.,

      make production NJOBS=20 NEV=1000 PZ=350

- <NJOBS>_<NEV>_<PZ>.jobs - run NJOBS simulation jobs, up to NSIM in
  parallel, each producing NEV events at a beam energy of PZ.  For example,
  to run 10 jobs, each with 1000 events at 350GeV/c and at most 5 jobs in
  parallel, do

      make 10_1000_350.jobs NSIM=5

  Negative numbers for NSIM means as many parallel jobs as available kernel
  threads (try "nproc") minus NSIM.   If you have 20 available kernel threads
  and you set NSIM=-2 then at most 18 jobs will be run in parallel.

- <NJOBS>_<PZ>_<NEV>.list - generates a list of input files for an
  analysis from the files generated by <NJOBS>_<PZ>_<NEV>.jobs.

- <NJOBS>_<PZ>_<NEV>.root - analysis the simulation output listed in
  <NJOBS>_<PZ>_<NEV>.list.

- clean - clean all built files, such as shared libraris, back-up
  files, logs, etc.  This _does not_ delete any generate ROOT files

- realclean - same as clean, but also deletes ROOT files

endef

.PHONY:build help
#
# EOF
#

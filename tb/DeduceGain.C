//____________________________________________________________________
TObject* GetObject(TDirectory* d, const char* name)
{
  if (!d) {
    Warning("GetObject", "No directory when looking for \"%s\"!",name);
    return 0;
  }
  TObject* o = d->Get(name);
  if (!o) 
    Warning("GetObject", "Object \"%s\" not found in \"%s\"",
	    name, d->GetName());
  return o;
}

//____________________________________________________________________
TMultiGraph* GetAdcMeanSD(TDirectory* d, Bool_t sum)
{
  TString  mgName(Form("%sStackStat", (sum ? "sumAdc" : "adc")));
  TObject* o = GetObject(d,mgName.Data());

  if (!o) return 0;

  return static_cast<TMultiGraph*>(o);
}

//____________________________________________________________________
TH1* GetElossDist(TDirectory* d, Bool_t sum=false)
{
  TString  dName = Form("FocalHDigitizer/%sEloss",
			(sum ? "sumAll" : "channel"));
  TObject* o     = GetObject(d,dName.Data());

  if (!o) return 0;

  return static_cast<TH1*>(o);
}

//____________________________________________________________________
void GetAdcMean(TMultiGraph* mg, Int_t i, Double_t& mean, Double_t& err)
{
  TGraphErrors* gm = static_cast<TGraphErrors*>(mg->GetListOfGraphs()->At(0));
  mean             = gm->GetPointY(i);
  err              = gm->GetErrorY(i);
}

//____________________________________________________________________
void GetAdcSD(TMultiGraph* mg, Int_t i, Double_t& sd, Double_t& err)
{
  TGraphErrors* gm = static_cast<TGraphErrors*>(mg->GetListOfGraphs()->At(1));
  sd               = gm->GetPointY(i);
  err              = gm->GetErrorY(i);
}

//____________________________________________________________________
void GetElossMean(TH1* h, Double_t& mean, Double_t& err)
{
  mean = h->GetMean();
  err  = h->GetMeanError();
}

//____________________________________________________________________
void GetElossSD(TH1* h, Double_t& mean, Double_t& err)
{
  mean = h->GetStdDev();
  err  = h->GetStdDevError();
}

//____________________________________________________________________
void SetAttr(TGraph* g, Color_t c, Style_t s, Double_t t=1)
{
  g->SetMarkerStyle(s);
  g->SetMarkerColor(c);
  g->SetLineColor  (c);
  g->SetMarkerSize (t);
}

//____________________________________________________________________
TGraphErrors* Ratios(TGraphErrors* g1, TGraphErrors* g2)
{
  Int_t mnn = TMath::Min(g1->GetN(), g2->GetN());
  TGraphErrors* g = new TGraphErrors(mnn);
  SetAttr(g, g1->GetLineColor(), g2->GetMarkerStyle(), g1->GetMarkerSize());
  
  for (Int_t i = 0; i < mnn; i++) {
    Double_t d  = g2->GetPointY(i);
    Double_t n  = g1->GetPointY(i);
    Double_t dx = g2->GetPointX(i);
    Double_t nx = g1->GetPointX(i);
    Double_t de = g2->GetErrorY(i) / d;
    Double_t ne = g1->GetErrorY(i) / n;
    Double_t r  = n / d;
    Double_t re = r * TMath::Sqrt(de*de + ne*ne);
    g->SetPoint(i, (dx+nx)/2, r);
    g->SetPointError(i, 0, re);
  }
  return g;
}

//____________________________________________________________________
void DrawTwo(TVirtualPad* p, TGraph* g1, TGraph* g2, Bool_t doScale=true)
{
  p->cd();
  p->SetLeftMargin(0.15);
  p->SetRightMargin(0.15);
  p->SetTopMargin(0.02);
  // g2->Draw("ap");
  // p->Clear();
  g1->Draw("ap");

  TF1* f1 = new TF1("f1","pol1");
  f1->SetLineColor(g1->GetLineColor());
  f1->SetLineStyle(2);
  g1->Fit(f1, "Q");
  
  Double_t xmx   = g1->GetHistogram()->GetXaxis()->GetXmax();
  Double_t mnp   = g1->GetHistogram()->GetMinimum();
  Double_t mxp   = g1->GetHistogram()->GetMaximum();
  Double_t mn2   = g2->GetHistogram()->GetMinimum();
  Double_t mx2   = g2->GetHistogram()->GetMaximum();

  Double_t scale = mxp / mx2;
  TGraph*  g3    = static_cast<TGraph*>(g2->Clone());
  // Printf("Axis range [%f,%f] second range [%f,%f] scale=%f",
  // 	 mnp,mxp,mn2,mx2,scale);
  if (scale * mn2 < mnp) {
    Printf("Change minimum from %f (> %f * %f) to = %f",
	   mnp, scale, mn2, scale * mn2);
    mnp = mn2 * scale;
    g1->GetHistogram()->SetMinimum(mnp);
  }
  g3->Scale(scale);
  g3->Draw("p");
  
  TF1* f3 = new TF1("f3","pol1");
  f3->SetLineColor(g3->GetLineColor());
  f3->SetLineStyle(2);
  g3->Fit(f3, "Q");

  TLegend* l  = p->BuildLegend(.3,.1,.85,.3);
  TAxis*   ya = g1->GetHistogram()->GetYaxis();
  ya->SetAxisColor(g1->GetMarkerColor());
  
  f1->Draw("same");
  f3->Draw("same");
  TGaxis *axis = new TGaxis(xmx,  mnp, xmx, mxp,
			    mnp / scale, mxp / scale,
			    ya->GetNdivisions(), "+L");
  Color_t col = g2->GetMarkerColor(); // ya->GetAxisColor();
  axis->SetLineColor  (col);
  axis->SetTitleColor (ya->GetTitleColor());
  axis->SetTitleFont  (ya->GetTitleFont());
  axis->SetTitleSize  (ya->GetTitleSize());
  axis->SetTitleOffset(2);//ya->GetTitleOffset());
  axis->SetLabelColor (ya->GetLabelColor());
  axis->SetLabelFont  (ya->GetLabelFont());
  axis->SetLabelSize  (ya->GetLabelSize());
  axis->SetLabelOffset(ya->GetLabelOffset());
  axis->SetTickLength (ya->GetTickLength());
  // axis->SetGridLength (-.74);
  axis->SetTitle(g2->GetHistogram()->GetYaxis()->GetTitle());
  // Printf("%f",axis->GetTitleOffset());
  axis->Draw();

}
//____________________________________________________________________
void SetTitles(TGraph* g, Bool_t mean=true, Bool_t adc=true, Bool_t sum=false)
{
  g->GetHistogram()->SetXTitle("#it{p}_{#it{z}} (GeV/c)");
  g->GetHistogram()->SetYTitle(Form("%s%s%s%s",
				    mean ? "#bar{" : "sd(",
				    sum  ? "#sum" : "",
				    adc  ? "ADC" : "#Delta",
				    !mean ? ")" : "}"));
  g->SetTitle(Form("%s of %s%s",
		   mean ? "Mean" : "S.D.",
		   sum  ? "sum " : "",
		   adc  ? "ADC" : "energy loss"));
}
//____________________________________________________________________
void SetRTitles(TGraph* g, Bool_t mean=true, Bool_t sum=false)
{
  g->GetHistogram()->SetXTitle("#it{p}_{#it{z}} (GeV/c)");
  g->GetHistogram()->SetYTitle(Form("%s%sADC%s/%s%s#Delta%s",
				    mean ? "#bar{" : "sd(",
				    sum  ? "#sum" : "",
				    !mean ? ")" : "}",
				    mean ? "#bar{" : "sd(",
				    sum  ? "#sum" : "",
				    !mean ? ")" : "}"
				    ));
  g->SetTitle(Form("Ratio of %s %sADC to %s %senergy loss",
		   mean ? "Mean" : "S.D.",
		   sum  ? "sum " : "",
		   mean ? "Mean" : "S.D.",
		   sum  ? "sum " : ""));

  Printf("==== %s ====", g->GetTitle());
  g->Print();
}
				    
  
void DeduceGain()
{
  TGraphErrors* adcMean    = new TGraphErrors();
  TGraphErrors* adcSD      = new TGraphErrors();
  TGraphErrors* sumAdcMean = new TGraphErrors();
  TGraphErrors* sumAdcSD   = new TGraphErrors();
  
  SetAttr(adcMean   ,kRed+1,20);
  SetAttr(adcSD     ,kRed+1,20);
  SetAttr(sumAdcMean,kGreen+2,20);
  SetAttr(sumAdcSD  ,kGreen+2,20);
  SetTitles(adcMean,    true,  true, false);
  SetTitles(adcSD,      false, true, false);
  SetTitles(sumAdcMean, true,  true, true);
  SetTitles(sumAdcSD,   false, true, true);
  

  TGraphErrors* elossMean    = new TGraphErrors();
  TGraphErrors* elossSD      = new TGraphErrors();
  TGraphErrors* sumElossMean = new TGraphErrors();
  TGraphErrors* sumElossSD   = new TGraphErrors();
  
  SetAttr(elossMean   ,kBlue+1,25,1.1);
  SetAttr(elossSD     ,kBlue+1,25,1.1);
  SetAttr(sumElossMean,kMagenta+2,25,1.1);
  SetAttr(sumElossSD  ,kMagenta+2,25,1.1);
  SetTitles(elossMean,    true,  false, false);
  SetTitles(elossSD,      false, false, false);
  SetTitles(sumElossMean, true,  false, true);
  SetTitles(sumElossSD,   false, false, true);
  
  std::map<unsigned short, const char*> adcFiles
    = { { 100, "tb/202211/noe/100GeV_dist.root" },
	{ 200, "tb/202211/noe/200GeV_dist.root" },
	{ 350, "tb/202211/noe/350GeV_dist.root" } };
  Int_t i = 0;
  for (auto efn : adcFiles) {
    TFile*       file  = TFile::Open(efn.second,"READ");
    TMultiGraph* adcMg = GetAdcMeanSD(file,false);
    TMultiGraph* sumMg = GetAdcMeanSD(file,true);
    Double_t     y, e;
    GetAdcMean(adcMg, 0, y, e);
    adcMean->SetPoint   (i, efn.first, y); adcMean   ->SetPointError(i, 0, e);
    GetAdcSD(adcMg, 0, y, e);
    adcSD->SetPoint     (i, efn.first, y); adcMean   ->SetPointError(i, 0, e);
    GetAdcMean(sumMg, 0, y, e);
    sumAdcMean->SetPoint(i, efn.first, y); sumAdcMean->SetPointError(i, 0, e);
    GetAdcSD(sumMg, 0, y, e);
    sumAdcSD->SetPoint  (i, efn.first, y); sumAdcMean->SetPointError(i, 0, e);
    i++;
  }

  std::map<unsigned short, const char*> elossFiles
    = { { 100, "vmc/data/g3_10_100_1000.root" },
	{ 200, "vmc/data/g3_10_200_1000.root" },
	{ 350, "vmc/data/g3_10_350_1000.root" }//,
  };
  i = 0;
  for (auto efn : elossFiles) {
    TFile*       file  = TFile::Open(efn.second,"READ");
    TH1*         elD   = GetElossDist(file);
    TH1*         sumD  = GetElossDist(file,true);
    Double_t     y, e;
    GetElossMean(elD, y, e);
    elossMean->SetPoint   (i,efn.first,y);elossMean   ->SetPointError(i,0,e);
    GetElossSD(elD, y, e);
    elossSD->SetPoint     (i,efn.first,y);elossMean   ->SetPointError(i,0,e);
    GetElossMean(sumD, y, e);
    sumElossMean->SetPoint(i,efn.first,y);sumElossMean->SetPointError(i,0,e);
    GetElossSD(sumD, y, e);
    sumElossSD->SetPoint  (i,efn.first,y);sumElossMean->SetPointError(i,0,e);
    i++;
  }

  TGraphErrors* ratMean    = Ratios(adcMean,   elossMean);
  TGraphErrors* ratSumMean = Ratios(sumAdcMean,sumElossMean);
  TGraphErrors* ratSD      = Ratios(adcSD,     elossSD);
  TGraphErrors* ratSumSD   = Ratios(sumAdcSD,  sumElossSD);
  SetRTitles(ratMean,    true,  false);
  SetRTitles(ratSumMean, true,  true);
  SetRTitles(ratSD,      false, false);
  SetRTitles(ratSumSD,   false, true);
  
  gStyle->SetOptTitle(0);
  
  TCanvas* c1 = new TCanvas("c1","c1",800*TMath::Sqrt(2),800);
  c1->Divide(3,2);
  // adcMean   ->SetTitle(Form("%s and energy loss",adcMean->GetTitle()));
  // sumAdcMean->SetTitle(Form("%s and energy loss",sumAdcMean->GetTitle()));
  // adcSD     ->SetTitle(Form("%s and energy loss",adcSD->GetTitle()));
  // sumAdcSD  ->SetTitle(Form("%s and energy loss",sumAdcSD->GetTitle()));
  DrawTwo(c1->cd(1), adcMean,    elossMean);
  DrawTwo(c1->cd(2), sumAdcMean, sumElossMean);
  DrawTwo(c1->cd(4), adcSD,      elossSD);
  DrawTwo(c1->cd(5), sumAdcSD,   sumElossSD);
  TVirtualPad* p = 0;
  p = c1->cd(3);
  p->SetRightMargin(0.02);
  p->SetTopMargin(0.02);
  TMultiGraph* ratMeans = new TMultiGraph;
  ratMeans->SetTitle("Ratio of ADC to energy loss mean and mean sum");
  ratMeans->Add(ratMean);
  ratMeans->Add(ratSumMean);
  ratMean->SetMarkerStyle(20); ratMean->SetMarkerSize(1);
  ratMeans->Draw("ap");
  p->BuildLegend(.2,.83,.98,.98);
  
  p = c1->cd(6);
  p->SetRightMargin(0.02);
  p->SetTopMargin(0.02);
  ratSD->SetMarkerStyle(20); ratSD->SetMarkerSize(1);
  TMultiGraph* ratSDs = new TMultiGraph;
  ratSDs->SetTitle("Ratio of ADC to energy loss S.D. and S.D.");
  ratSDs->Add(ratSD);
  ratSDs->Add(ratSumSD);
  ratSDs->Draw("ap");
  p->BuildLegend(.2,.83,.98,.98);
  // DrawTwo(c1->cd(3), ratMean,    ratSumMean);
  // DrawTwo(c1->cd(6), ratSD,      ratSumSD);

  c1->Modified();
  c1->Update();
  c1->cd();
  c1->SaveAs("gain.pdf");
  
}

  

#
# Only does clean-up 
#
SUBDIRS		:= common shared vmc g4 tb misc

help:; @$(info $(helpmsg)) :

clean:
	$(foreach d, $(SUBDIRS), $(MAKE) -C $(d) clean;)

realclean:
	$(foreach d, $(SUBDIRS), $(MAKE) -C $(d) realclean;)


define helpmsg = 
- HOW TO USE MAKE WITH THIS PROJECT

    make TARGET [VAR=VALUE]*

"generates" TARGET, which must be a valid target. If not TARGET is
given, it defaults to "help".  A variable VAR can be set to a VALUE
directly on the command line.

Valid targets for this project are

- help - show this help

- clean - clean all built files, such as shared libraris, back-up
  files, logs, etc.  This _does not_ delete any generate ROOT files

- realclean - same as clean, but also deletes ROOT files

More targets for VMC, Geant4, test beam data, etc. are available in
relevant sub-directories.
endef


.PHONY:	help

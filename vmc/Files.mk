#
# The files
#
cmndir		:= $(srcdir)../common/
DATA_SOURCES	:= $(cmndir)FocalH.C		\
		   $(cmndir)FocalE.C		\
		   $(srcdir)FocalHHit.C		\
		   $(srcdir)FocalEHit.C		\
		   $(cmndir)FocalHDigit.C	\
		   $(cmndir)FocalHSDigit.C	\
		   $(srcdir)FocalHFlux.C	\
		   $(srcdir)FocalHSum.C		\
		   $(cmndir)FocalHRO.C		

SIM_SOURCES	:= $(srcdir)Simulate.C		\
		   $(cmndir)ScintOptical.C	\
		   $(cmndir)AirOptical.C	\
		   $(srcdir)Builder.C		\
		   $(srcdir)SubBuilder.C	\
		   $(srcdir)FrontBack.C		\
		   $(srcdir)FocalHBuilder.C	\
		   $(srcdir)FocalEBuilder.C	\
		   $(srcdir)FocalHHitter.C	\
		   $(srcdir)FocalEHitter.C	\
		   $(srcdir)FocalHFluxer.C	\
		   $(srcdir)FocalHSummer.C		

ANA_SOURCES	:= $(cmndir)Analyse.C		\
		   $(cmndir)Analyser.C		\
		   $(srcdir)Profiler.C		\
		   $(srcdir)Leaving.C		\
		   $(srcdir)FocalHDigitizer.C		

ATT_SOURCES	:= $(cmndir)Attenuate.C			\
		   $(shrdir)Attenuator.C		\
		   $(cmndir)AttConfig.C			\
		   $(cmndir)LoadAtt.C			

SCRIPTS		:= $(srcdir)CompareProfiles.C	\
		   $(srcdir)LoadData.C		\
		   $(srcdir)LoadSim.C		\
		   $(cmndir)LoadAna.C
#
# EOF
#

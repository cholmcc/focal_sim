#
#
#

all:	focalh

Makefile:	../source/CMakeLists.txt
	cmake -DCMAKE_CXX_STANDARD=20 ../source


focalh:	Makefile
	$(MAKE) -f Makefile

clean:
	-$(MAKE) -f Makefile $@
	rm -f *~  *.log

realclean:clean
	rm -f cmake_install.cmake
	rm -f CMakeCache.txt
	rm -f Makefile	
	rm -f *.mac *.root
	rm -rf CMakeFiles 

#
#
#

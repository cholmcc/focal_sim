# Simulation of FoCAL test-beam setup 

![Event](.event.webp "Event")

A similar project, which uses Geant 4 directly can be found in
top-level sub-directory [`g4`](../g4).

## Content 

### Geometry building

- [`Builder.C`](Builder.C) - this contains the class `Builder`, which
  is a `Framework::Task` that defines the geometry of the setup.  It
  does not take care of hits in the detector - that is done by the
  classes `FocalHHitter`, `FocalEHitter`, `FocalHFluxer`, and
  `FocalHSummer`.
- [`FocalHBuilder.C`](FocalHBuilder.C) builds the FoCAL-H geometry.
  It is also a `Framework::Task` that is added to the `Builder` task. 
- [`FocalEBuilder.C`](FocalEBuilder.C) builds the FoCAL-H geometry.
  It is also a `Framework::Task` that is added to the `Builder` task. 
  
### Data structures 

- [`FocalHHit.C`](FocalHHit.C) a hit in FoCAL-H.  Encodes energy loss,
  particle type, particle momentum, position, simulated light
  yield in the hit step, and the straw identifier. The objects contain 
  - Track number that caused the hit 
  - PDG identifier of the particle type
  - Spatial location of the hit $`(x,y,z,t)`$
  - Momentum of the particle at the hit $`(p_x,p_y,p_z,E)`$
  - Energy loss 
  - The volume number of the straw in which the hit was produced. 
- [`FocalEHit.C`](FocalEHit.C) a hit in FoCAL-E.  Encodes energy loss,
  particle type, particle momentum, position, and pad or pixel
  identifier. The objects contain 
  - Track number that caused the hit 
  - PDG identifier of the particle type
  - Spatial location of the hit $`(x,y,z,t)`$
  - Momentum of the particle at the hit $`(p_x,p_y,p_z,E)`$
  - Energy loss 
  - The volume number of the pad or pixel in which the hit was
	produced.  
- [`FocalHSum.C`](FocalHSum.C) an integrated hit in each of the
  FoCAL-H straws.  This records the straw number, summed energy loss,
  summed light-yield, and number of photons that exit the straw
  through the back. The objects contain 
  - Total energy loss in a straw
  - Number of hits that contributed to the energy loss
- [`FocalHFlux.C`](FocalHFlux.C) records the type of particles that
  exit the FoCAL-H volume through the back.  This encodes the particle
  type and whether the particle entered or left the volume.  The
  objects contain
  - Track number that caused the hit 
  - PDG identifier of the particle type
  - Spatial location of the hit $`(x,y,z,t)`$
  - Momentum of the particle at the hit $`(p_x,p_y,p_z,E)`$
  - Flags which says whether track entered or exited the set-up. 

### Sensitive detectors 

- [`FocalHHitter.C`](FocalHHitter.C) - this contains the class
  `FocalHHitter`, which is a `Simulation::Task` that records hits in
  the detector volume.  The hits are objects of the class `FocalHHit`.
- [`FocalEHitter.C`](FocalEHitter.C) - this contains the class
  `FocalEHitter`, which is a `Simulation::Task` that records hits in
  the detector volume.  The hits are objects of the class `FocalEHit`.
- [`FocalHSummer.C`](FocalHSummer.C) - this contains the class
  `FocalHSummer` which is a `Simulation::Task` to sum energy loss in a
  single scintillating fibre.  The class has a parameter that
  determines the integration time (in seconds).  If a hit is later,
  including the time it will take the light to reach the end-cap, than
  this cut off, then the hit is not included in the sum.  This creates
  objects of class `FocalHSum`
- [`FocalHFluxer.C`] - another `Simulation::Task` derived class that
  records the particle types that enter or exit the FoCAL-H volume
  through the front or  back, respectively. 
  
### Steering 

- [`SimConfig.C`](SimConfig.C) - configuration of the simulation.  In this
  script the various tasks above are added to the simulation chain,
  and the event generator is defined.  
  - This is also where we set the simulation back-end (GEANT 3.21 or
	Geant 4) to use in the simulation.  The simulation backend is
	selected by a integer argument (0 means GEANT 3.21, 1 is Geant 4,
	2 is Geant 4 with native navigation).
  - The event generator is set up to produce single positive charged
	pions with a configurable momentum in the Z direction (default to
	50 GeV).
  - The production vertex is set to be smeared by a standard deviation
    of 1mm in both X and Y.
  - By default, the event display is enabled if the simulation is run
    in interactive mode (i.e., not passed `-b` to ROOT)
  - By default, the tasks `FocalHHitter`, `FocalEHitter`,
    `FocalHFlexer`, and `FocalHSummer` are added to the simulation.
    These can be turned off by calling `task->SetActive(false)` in
    this script.
- [`Simulate.C`](Simulate.C) - script to run the simulation.  This is
  the main user entry point for the simulation.  From here, we can
  choose
  - Number of events to generate.  If `0` is passed, then the
    simulation chain is simply set-up but not executed, and an
    interactive ROOT session is started.
  - Momentum along Z of the produced particles, in GeV/c. 
  - Simulation back-end - either GEANT 3.21 (0), Geant 4 (1), or Geant
    4 with native navigation. 
  - Whether to force an interactive session even if the number of
    events is non-zero.  This is useful if we want to produce a few
    events and inspect them in the event display. 
  - Output file name.  If set to `auto` then the file name is computed
    from the settings. 
	
### Other 

- [`CompareProfiles.C`](CompareProfiles.C) - compare the profiles
  generated by `Profiler` from two different runs - for example one
  using GEANT 3.21 and the other using Geant 4.
- [`Makefile`](Makefile) - a recipe file for _Make_ to do various
  chores.  
- [`README.md`](README.md) - this file 
- [`data`](data) A sub-directory set-up to produce our data in.  See
  more below.

## Usage 

_Do not sh*t where you eat_ 

The above is generally a good advice, also in the case of software.
What it means is that we do not put our messy stuff with the stuff we
want to work with.  That is, we want to keep our sources and the data
we generate with the sources separate from each other.  To that end,
this project provides the [`data`](data) sub-directory.   Change
directory to that sub-directory, and you should run the code from
there. (The [`data/.rootrc`](data/.rootrc) in that directory sets up
ROOT's script path to look in the source directory). 

### Simulations

The [`data/Makefile`](data/Makefile) contains a number of targets to
help use the code.  Do

	$ make help 
	
to see more.

If you prefer, you can also run simulations _by-hand_.  For example 

	$ root Simulate.C\(10,200,1\) 
	
Will generate 10 events with a momentum of $`200\,\mathrm{GeV}`$ using
the Geant 4 back-end.   The same can be accomplished with 

	$ make g4_10_200_events.root

Although the [`doc/Makefile`](doc/Makefile) provides a single target
for analysis, we may want to run scripts or the like on the generated
data.  The project provides the script [`LoadData.C`](LoadData.C)
which will load data classes into ROOT.  Make sure to execute that
script before inspecting the generated data.  For example, we could do

	$ root LoadData.C events.root 
	Root> T->StartViewer()
	
to inspect the event tree. 

### Analyses 

Generally, an analysis of the generated data should be derived from
the base class [`Analyser`](../common/Analyser.C).  Such a class
should be put in a file in the source directory.  Examples of such
analyses are [`Profiler`](../shared/Profiler.C),
[`Leaving`](../shared/Leaving.C), and
[`FocalHDigitizer`](../shared/FocalHDigitizer.C). 

The [`data/Makefile`](data/Makefile) contains some targets for
analyses.  For example 

	make analysis 
	
will run an analysis on the simulation output file `events.data`.
Doing 

	make g3_200_1000_analysis.root 
	
will run the same analysis on the simulation output file
`g3_200_1000_events.root`.   You can also run the analyses defined in
[`AnaConfig.C`](AnaConfig.C) by-hand, for example on the simulation
file `g4_500_1000_events.root`, with

	root Analyse.C\(-1,\"g4_500_1000_events.root\"\) 
	
which will produce `g4_500_1000_analysis.root`.

#### Adding an analysis 

- Define a class `MyAnalysis` deriving from `Analyser`

	  // MyAnalysis
      #include "Analyser.C"
	  class TH1;
	  
      class MyAnalysis : public Analyser
	  {
	  public:
	    MyAnalysis(const char* writerName)
		  : Analyser("MyAnalysis",writerName)
	    {}
	    void Register(Option_t* option="") {
		  Analyser::Register();
		  fH = new TH1("h",h",100,0,10);
		  fFolder->Add(fH);
	    }
		void Exec(Option_t* option) {
		  TClonesArray* sums = GetSums();
		  while (auto o : *sums) { 
		    Sum* sum = static_cast<Sum*>(o);
			fH->Fill(sum.fEnergyLoss);
		  }
		}
		void Finish(Option_t* option) {
		  TDirectory* file = FindDirectory(Form("%s/file",GetTitle()));
		  WriteFolder(file,false);
	    }
      };
- Make sure `MyAnalysis.C` is loaded in [`AnaLoad.C`](AnaLoad.C). 
- Make an instance of `MyAnalysis` in `AnaConfig` and add that object
  to the main steering object. 
- You can now run this analysis just as described above. 

### Run somewhere entirely different

If you want to run the code somewhere entirely different - which is
strongly encourage - say, for example `~/FocalHSimulation/data/g3`,
then copy [`data/.rootrc`](data/.rootrc) and
[`data/Makefile`](data/Makefile) to that directory 

	$ cp data/.rootrc data/Makefile ~/FocalHSimulation/data/g3

Then open both files in an editor and change them appropriately.  For
example, if you had put the sources in `~/FocalHSimulation/src`, then
`~/FocalHSimulation/data/g3/.rootrc` should have 

	Unix.*.Root.DynamicPath: $(HOME)/FocalHSimulation/src
	Unix.*.Root.MacroPath:   $(HOME)/FocalHSimulation/src
	
and in `~/FocalHSimulation/data/g3/Makefile` you should have 

	srcdir          := $(HOME)/FocalHSimulation/src
	
If you do not want to put your code in the source directory, you can
follow this strategy.  For example, if you have your code in
`~/FocalHSimulation/tests`, then your `.rootrc` file could read 

	Unix.*.Root.DynamicPath: $(HOME)/FocalHSimulation/src:$(HOME)/FocalHSimulation/test
	Unix.*.Root.MacroPath:   $(HOME)/FocalHSimulation/src:$(HOME)/FocalHSimulation/test

## Timing 

To do timing comparisons, use the included `Makefile`.  First, we
should build the code so that it is not part of timing 

	$ make build
	
Then we should run jobs as appropriate.  For example 

	$ make g3_200_100_events.root > g3.log 2>&1
	$ make g4_200_100_events.root > g4.log 2>&1 
	$ make n4_200_100_events.root > n4.log 2>&1 
	
which will produce 100 events with $`p_z=200\,\mathrm{GeV}`$ $`\pi^-`$
with both GEANT 3.21 and Geant 4.  The output is written to `g3.log`
and `g4.log`.  At the very end of those log files, you will see the
timing estimates. 

With the above, we see for GEANT 3.21 

    Command being timed: "root -l --web=off -q Simulate.C(100,200,0,0)"
    User time (seconds): 59.13
    System time (seconds): 0.19
    Percent of CPU this job got: 99%
    Elapsed (wall clock) time (h:mm:ss or m:ss): 0:59.41

For Geant 4 (`TGeo` navigation), we find 

    Command being timed: "root -l --web=off -q Simulate.C(100,200,1,0)"
    User time (seconds): 279.25
    System time (seconds): 0.28
    Percent of CPU this job got: 99%
    Elapsed (wall clock) time (h:mm:ss or m:ss): 4:39.71

In this setup, Geant 4 is almost 4.5 times slower at ~4.5 seconds per
event, while GEANT 3.21 is at 2.8 seconds per event.  With native
Geant 4 navigation we get

    Command being timed: "root -l --web=off -q Simulate.C(100,200,2,0)"
    User time (seconds): 269.33
    System time (seconds): 0.24
    Percent of CPU this job got: 99%
    Elapsed (wall clock) time (h:mm:ss or m:ss): 4:29.75
	
roughly the same as Geant 4 with ROOT `TGeo` navigation.

A pure Geant 4 [implementation](https://gitlab.com/cholmcc/focalh_g4)
gives

    Command being timed: "../main -t pi- -p 200 -n 100"
    User time (seconds): 183.12
    System time (seconds): 0.31
    Percent of CPU this job got: 99%
    Elapsed (wall clock) time (h:mm:ss or m:ss): 3:03.62

or roughly 1.8 seconds per event. 

| Simulation library | Seconds per Event | xG3 | xG4 |
|--------------------|-------------------|-----|-----|
| `TGeant3`          | 0.6               | =1  | 0.2 |
| `TGeant4` (`TGeo`) | 2.8               | 4.7 | 1.6 |
| `TGeant4` (G4)     | 2.7               | 4.5 | 1.5 |
| Geant 4            | 1.8               | 3   | =1  |

Note that `TVirtualMC` application actually does a bit more than the
pure Geant 4 implementation (`Fluxer` for example), so some of the
overhead can be attributed to that (but probably not all of it). 

## Profiling 

To run the code with profiling, do for example 

	$ make g3_200_100_events.root PROFILE=1
	$ make g4_200_100_events.root PROFILE=1
	$ make n4_200_100_events.root PROFILE=1
	
and then investigate the profiling output with for example `hotspot`. 

This allowed me to identify where the problematic code in
`root-simulation` was (call to `TObjArray::GetEntries` rather than
`TObjArray::GetEntriesFast`). 

At a quick glance, it looks like some of the `TGeant4` overhead
(wrt. pure Geant 4) does come from the geometry navigator, but also
from the stepping code. 

## Changing configurations 

To change backend settings, copy `MoreConfig.C` from the
`root-simulation` installation

	$ cp `simulation-config --prefix`/share/simulation/MoreConfig.C . 
	
and edit as appropriate.  

For specific backends we need to copy the specific scripts

	$ cp `simulation-config --prefix`/share/simulation/G3Config.C . 
	$ cp `simulation-config --prefix`/share/simulation/G4Config.C . 
	
and edit those as appropriate.  For example, if we need a different
physics list for Geant 4, we should copy `G4Config.C` to the working
directory (or some directory in ROOT's macro search path) and edit as
appropriate. 

## Analysis 

The class `Analyser` in [`Analyser.C`](../common/Analyser.C) can serve
as base class for analysis code.  The class provides utilities to
retrieve hits, flux-objects, and tracks from the input.

An example of an analysis class is `Profiler` in
[`Profiler.C`](../shared/Profiler.C).

An analysis pass is configured much the same way a simulation is
configured (the `Config.C` script).  One creates a script that sets up
the needed tasks - _a la_ [`AnaConfig.C`](../common/AnaConfig.C). This
script can make objects of user defined analysis tasks, like for
example `Profiler`.

One can then make a steering script, just like the `Simulate.C` script
for the simulations, where one makes sure to compile all the needed
code and then call the configuration script.  An example of such as
steering script can be found in [`Analyse.C`](../common/Analysis.C).

## Animation 

- Run simulation with interactive interface 

      $ make run NEV=10 
	
- When you find an event you like to animate, adjust the time slider
  at the bottom of the window to match time you want to animate
  (roughly between $`t=0`$ and $`t=1\times10^{-9}`$).
- Then press the *Animate* button to see if the time window is OK.
  - If it isn't, click the *Animate* button to disable animations and
  then adjust the time slider again.  
- When you are happy with the time window, make sure *Animate* is
  down, and then press *Record* 
  - This will generate 100 GIF images in the current directory.
- Now open Gimp 

	  $ gimp 
	  
  - From the *File* menu select _Open as layers..._ and select all
    the `anim*.gif` files and press *OK*. 
  - The images will be imported in reverse order.  From the *Layer*
    menu select *Stack*->*Reverse layer order* to reorder the layers. 
  - From the *Image* menu select *Mode*->*RGB*.
  - Then, from the *File* menu select *Export as...* 
  - In the file dialog type in the name of the file you want to save
    the animation as, making sure that the file name ends in `.webp`
    or `.gif` - for example `event.webp`.  Then press *OK*. 
  - In the save options dialog that pops up, enable *As Animation* and
    adjust other settings to your licking (I prefer 100 ms between
    frames).  Then press *Export* 
	
- Voila - you have an animation :-)

## Notes 

- https://en.wikipedia.org/wiki/Cylinder#Cylindric_sections
  
  Semi major axis $`A`$ given by $`A=R/\sin\alpha`$ where
  $`\alpha=90^\circ`$ is perpendicular to the cylinder axis
  ($`z`$-axis).  
  
  Fibre $`[c,r]` collected must thus be placed $`A`$ from the closest
  circumference of another fibre.
  
  
#
# EOF
#

#include "Geometry.hh"
#include "UserActionInitialization.hh"
#include "OpticalPhotonPhysics.hh"
#include "RootIO.hh"

#include "G4RunManager.hh"
#include "G4UImanager.hh"
#include "G4VisExecutive.hh"
#include "G4UIExecutive.hh"
#include "QGSP_BERT.hh"

// If you want to visualize the setup:
//  1. set nameUISession = "qt"
//  2. uncomment ui->SessionStart()
//  3. Uncomment /control/execute visSetup_Simplex.mac in GlobalSetup.mac


//--------------------------------------------------------------------
void usage(const std::string& progname, std::ostream& o=std::cout)
{
  o << "Usage: " << progname << " [OPTIONS] [SCRIPT]\n\n"
    << "Options:\n"
    << "  -h         This help\n"
    << "  -s INT INT Set seeds\n"
    << "  -b SCRIPT  Batch script to run\n"
    << "  -o FILE    Output file\n"
    << "  -i         Interactive session\n"
    << "  -I DIR     Add macro directory\n"
    << "  -U UI      UI type\n"
    << "  -V VIS     Visualisation type\n"
    << "  -Q         Use Qt UI and visualisation\n"
    << "  -v         Increase verbosity\n\n"
    << "Options '-v' and '-I' can be given multiple times\n"
    << "SCRIPTS is a list of scripts to execute\n"
    << "UI can be one of [t]csh, Qt(3D), Xm, ...\n"
    << "VIS can be on of Qt3D, OGL, OGLX, OGLQt, OGLXm, TSG_XT_GLES, TSG_QT_GLES, TSG_X11_GLES, Raytrace,...\n"
    << std::endl;
}

//--------------------------------------------------------------------
int main( int argc, char** argv )
{
  // ============= [ Setting up the application environment ] ================
  G4String    uiType      = "tcsh";             // UI Session (qt/tcsh/...)
  G4String    visType     = "OGL";
  G4String    batchScript = "single.mac";  // Initialization  macros
  bool        interactive = false;
  bool        qt          = false;
  std::string output      = "hits.root";
  std::vector<G4String> macros;
  std::vector<G4String> paths;
  int nEvents = 1;
  int verbose = 0;
  int seed1   = 41242;
  int seed2   = 87053;
  
  for (int i = 1; i < argc; i++) {
    std::cout << "Process command line argument " << argv[i] << std::endl;
    if (argv[i][0] == '-') {
      switch (argv[i][1]) {
      case 'h': usage(argv[0]); return 0;
      case 's':
        seed1 = std::stoi(argv[++i]);
        seed2 = std::stoi(argv[++i]);
        break;
      case 'b': batchScript = argv[++i];        break;
      case 'v': verbose += 1;                   break;
      case 'i': interactive = true;             break;
      case 'I': paths.push_back(argv[++i]);     break;
      case 'U': uiType = argv[++i];             break;
      case 'V': visType = argv[++i];            break;
      case 'Q': qt = true;                      break;
      case 'n': nEvents = std::stoi(argv[++i]); break;
      case 'o': output = argv[++i];             break;
      default:
        std::cout << argv[0] << ": Unknown option: " << argv[i]
                  << std::endl;
        return 1;
      }
    }
    else
      macros.push_back(argv[i]);
  }
  if (interactive) {
    if (qt) {
      uiType  = "Qt";
      visType = "Qt3D";
    }
  }
  
  // Create output
  if (not RootIO::GetInstance()->Open(output)) {
    std::cerr << "Failed to open output file " << output << std::endl;
    return 1;
  }
  
  // Construct the default run manager
  auto runManager = new G4RunManager;
  
  // Set up mandatory user initialization: Geometry
  runManager->SetUserInitialization( new Geometry() );
  
  // Set up mandatory user initialization: Physics-List
  auto physicsList = new QGSP_BERT;
  physicsList->RegisterPhysics( new OpticalPhotonPhysics() );
  runManager->SetUserInitialization( physicsList );
  
  // Set up user initialization: User Actions
  runManager->SetUserInitialization( new UserActionInitialization() );

  // Initialize G4 kernel
  runManager->Initialize();

  // Create visualization environment
  auto visManager = new G4VisExecutive;
  visManager->Initialize();

  // Start interactive session
  auto uiManager     = G4UImanager::GetUIpointer();
  auto uiInteractive = new G4UIExecutive(argc, argv, uiType);

  // Seeds 
  uiManager->ApplyCommand("/random/setSeeds "+std::to_string(seed1)+" "+
                          std::to_string(seed2));

  // Verbosity 
  std::map<std::string,int> levels = {
    {"/run/verbose",                          1},
    {"/tracking/verbose",                     2},
    {"/control/verbose",                      2},
    {"/event/verbose",                        2},
    {"/material/verbose",                     3},
    {"/process/verbose",                      3},
    {"/vis/verbose",                          4},
    {"/cuts/verbose",                         5},
    {"/geometry/navigator/verbose",           5},
    {"/particle/verbose",                     5},
    {"/particle/process/verbose",             5},
    {"/process/eLoss/verbose",                5},
    {"/process/had/verbose",                  5},
    {"/process/emp/verbose",                  5},
    {"/process/optical/verbose",              5},
    {"/process/optical/scintilation/verbose", 6},
    {"/process/optical/cerenkov/verbose",     6},
    {"/process/optical/boundary/verbose",     6},
    {"/process/optical/absorbtion/verbose",   6},
  };
  for (auto l : levels) 
    uiManager->ApplyCommand(l.first+" "+
                            std::to_string(std::max(verbose - l.second,0)));

  // Macro paths
  std::string mp = ".";
  for (auto p : paths) mp += ":" + p;
  uiManager->ApplyCommand("/control/macroPath "+mp);

  // Macros 
  for (auto m : macros) 
    uiManager->ApplyCommand("/control/execute " + m);

  // Interactive 
  if (interactive) {
    std::vector<std::string> cmds{
      "/vis/open "+visType,
      "/vis/drawVolume world",
      // "/vis/scene/add/trajectories smooth",
      // "/vis/scene/endOfEventAction accumulate",
      // "/vis/viewer/set/background white",
      // "/vis/scene/add/axes",
      // "/vis/viewer/set/style surface", // wireframe
      // "/vis/viewer/set/edge true",
      // "/vis/viewer/set/auxiliaryEdge true",
      // "/vis/viewer/set/lineSegmentsPerCircle 100",
      // "/vis/viewer/set/viewpointThetaPhi 120 150"
      ""
    };
    for (auto c : cmds)
      if (c != "") uiManager->ApplyCommand(c);
    uiInteractive->SessionStart();
  }
  else if (not batchScript.empty())
    uiManager->ApplyCommand("/control/execute " + batchScript);
    

  RootIO::GetInstance()->Close();
  
  // Job termination
  // delete uiInteractive;
  // delete visManager;
  // delete runManager;
  
  return 0;
}
//
// EOF
//

# Building the setup 

## General observations 

We will use stand-alone installations - that is, we will _not_ require
the bloated `aliDist` stuff or the like.  Also we will try not to s**t
where we eat, which means keeping source code, builds, and
installations separate. 

We will assume a root directory for our stuff - namely 

	/opt/sw
	
with sources, builds, and installations in 

	/opt/sw/src
	/opt/sw/build
	/opt/sw/inst 
	
respectively.   Change `/opt/sw` to your preferred prefix. 

## ROOT 

To get the ROOT sources, do 

	$ cd /opt/sw/src 
	$ git clone https://github.com/root-project/root.git
	$ cd root 
	$ git checkout latest-stable 
	
To build, do 

	$ cd /opt/sw/build
	$ mkdir root 
	$ cd root 
	$ cmake ../../src/root -DCMAKE_INSTALL_PREFIX=/opt/sw/inst \
                           -DCMAKE_CXX_STANDARD=17 \
						   -DCXX_STANDARD=17
	$ make 
	
To install, do 

	$ cd /opt/sw/build/root 
	$ make install 
	

## Interlude - make shell config script 

We make a shell initialisation script which we will source when we
need to use ROOT, etc.

	$ cd /opt/sw/inst/etc 
	$ vi profile 
	
The `profile` file should have content like 

    #!/bin/sh
    #
    # Source for setting up use of applications, etc. in this directory
    #
    INST_DIR=`dirname $(dirname $BASH_SOURCE)`
    # echo $INST_DIR
    
    # --------------------------------------------------------------------
    #
    # ROOT and other stuff
    #
    if test -f $INST_DIR/bin/thisroot.sh ; then
       . $INST_DIR/bin/thisroot.sh
    
       # Clean up some extra stuff set by the above
       unset SHLIB_PATH
       unset DYLD_LIBRARY_PATH
       unset CMAKE_PREFIX_PATH
       unset LIBPATH
       unset JUPYTER_PATH
    elif test -d ${INST_DIR}/bin && -d ${INST_DIR}/lib ; then 
        export PATH=${PATH}:${INST_DIR}/bin
        export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${INST_DIR}/lib
    fi
	
    # --------------------------------------------------------------------
    #
    # Python stuff
    #
    pyvers=`python --version | sed -e s'/.* //' -e s/.[0-9]$//`
    pydir=$INST_DIR/lib/python${pyvers}/site-packages
    
    if test -d $pydir ; then
       PYTHONPATH=${PYTHONPATH}:${INST_DIR}/lib:${pydir}
    fi

When you need to use ROOT etc., simply do 

	$ . /opt/sw/inst/etc/profile 
	
Alternatively you can make an alias that does this for your. 

	$ vi ~/.bashrc 
	
and add the line 

	alias sw=source /opt/sw/inst/etc/profile 
	
Now you can simply type 

	$ sw 
	
to set-up your shell to use ROOT etc.

In the following, we will assume you have sourced
`/opt/sw/inst/etc/profile`. 

## ROOT VMC 

To get the ROOT VMC sources, do 

	$ cd /opt/sw/src
	$ git clone git@github.com:vmc-project/vmc.git 

To build ROOT VMC, do 

	$ cd /opt/sw/build
	$ mkdir vmc 
	$ cd vmc 
    $ cmake ../../src/vmc -DCMAKE_INSTALL_PREFIX=/opt/sw/inst \
                          -DCMAKE_CXX_STANDARD=17 \
                          -DCXX_STANDARD=17
    $ make 
	
To install, do 

	$ cd /opt/sw/build/vmc
	$ make install 
	
## ROOT Framework 

To get the ROOT Framework sources, do 

	$ cd /opt/sw/src 
	$ git clone git@gitlab.cern.ch:cholm/root-framework.git
	
To build the ROOT Framework sources, do 

	$ cd /opt/sw/build
	$ mkdir root-framework 
	$ cd root-framework 
	$ ../../src/root-framework/configure --prefix=/opt/sw/inst
	$ make 
	
To install ROOT Framework, do 

	$ cd /opt/sw/build/root-framework 
	$ make install 

## ROOT Simulation 

To get the ROOT Simulation sources, do 

	$ cd /opt/sw/src 
	$ git clone git@gitlab.cern.ch:cholm/root-simulation.git
	
To build the ROOT Simulation sources, do 

	$ cd /opt/sw/build
	$ mkdir root-simulation 
	$ cd root-simulation 
	$ ../../src/root-simulation/configure --prefix=/opt/sw/inst
	$ make 
	
To install ROOT Simulation, do 

	$ cd /opt/sw/build/root-simulation 
	$ make install 
	
## Geant 3.21 

This only need ROOT VMC.  

To get the Geant 3.21 (VMC) sources, do 

	$ cd /opt/sw/src
	$ git clone git@github.com:vmc-project/geant3.git
	
To build Geant 3.21 (VMC), do 

	$ cd /opt/sw/build 
	$ mkdir geant3 
	$ cd geant3 
    $ cmake ../../src/geant3 -DCMAKE_INSTALL_PREFIX=/opt/sw/inst \
                             -DCMAKE_CXX_STANDARD=17 \
                             -DCXX_STANDARD=17 \
                             -DVMC_DIR=/opt/sw/inst/lib/VMC-2.0.0 \
                             -DGeant3_INSTALL_DATA=YES
						  
To install Geant 3.21 (VMC), do 

	$ cd /opt/sw/build/geant3 
	$ make install 

## Geant 4 

Geant 4 needs Geant 4 proper, VGM, and Geant 4 VMC 

### Geant 4 proper 

If you want a GUI (Graphical User Interface), make sure to install
Qt development packages from your Linux distribution 

We will rely on the Qt 3D library, so you may need (on Debian and
derivatives such as Ubuntu) 

	$ sudo apt install qt3d5-dev 
	
To get Geant 4 sources, do 

	$ cd /opt/sw/src 
	$ git clone git@github.com:Geant4/geant4.git

To build Geant 4, do 

    $ cd /opt/sw/build
    $ mkdir geant4 
    $ cd geant4 
    $ cmake ../../src/geant4 -DCMAKE_INSTALL_PREFIX=/opt/sw/inst \
                             -DCMAKE_CXX_STANDARD=17 \
                             -DCXX_STANDARD=17 \
                             -DGEANT4_INSTALL_DATA=YES \
                             -DGEANT4_USE_G3TOG4=YES \
                             -DGEANT4_USE_GDML=YES \
                             -DGEANT4_BUILD_TLS_MODEL=global-dynamic \
							 -DGEANT4_USE_QT=YES
    $ make 
	
To install Geant 4, do 

	$ cd /opt/sw/build/geant4 
	$ make install 
	
and possibly 

	$ geant4-config --install-datasets

### Interlude 

At this point, we need to add a bit to `/opt/sw/inst/etc/profile`.
Open that file in an editor, and _add_ the lines 

    # --------------------------------------------------------------------
    #
    # Geant4 stuff
    #
    g4_data=${INST_DIR}/share/Geant4-11.0.2/data
    export G4NEUTRONHPDATA="${g4_data}/G4NDL4.6"
    export G4LEDATA="${g4_data}/G4EMLOW8.0"
    export G4LEVELGAMMADATA="${g4_data}/PhotonEvaporation5.7"
    export G4RADIOACTIVEDATA="${g4_data}/RadioactiveDecay5.6"
    export G4PARTICLEXSDATA="${g4_data}/G4PARTICLEXS4.0"
    export G4PIIDATA="${g4_data}/G4PII1.3"
    export G4REALSURFACEDATA="${g4_data}/RealSurface2.2"
    export G4SAIDXSDATA="${g4_data}/G4SAIDDATA2.0"
    export G4ABLADATA="${g4_data}/G4ABLA3.1"
    export G4INCLDATA="${g4_data}/G4INCL1.0"
    export G4ENSDFSTATEDATA="${g4_data}/G4ENSDFSTATE2.3"
	
or 

    # --------------------------------------------------------------------
    #
    # Geant4 stuff
    #
	eval `geant4-config --datasets | cut -f2,3 -d' ' | \
		  sed 's/\([^[:space:]]*\) *\/*/export \1=\//'` 
		  
which will use the `geant4-config` script to set the variables 
    
Again, in the flowing, we will assume you have sourced
`/opt/sw/inst/etc/profile` with this new content. 

### ROOT VGM 

To get the ROOT VGM sources, do 

	$ cd /opt/sw/src
	$ git clone git@github.com:vmc-project/vgm.git 
	
To build ROOT VGM, do 

	$ cd /opt/sw/build
	$ mkdir vgm 
	$ cd vgm 
    $ cmake ../../src/vgm -DCMAKE_INSTALL_PREFIX=/opt/sw/inst \
                          -DCMAKE_CXX_STANDARD=17 \
                          -DCXX_STANDARD=17 \
                          -DWITH_GEANT4=YES \
                          -DWITH_GEANT4_UIVIS=YES \
                          -DWITH_ROOT=YES

To install ROOT VGM, do 

	$ cd /opt/sw/build/vgm 
	$ make install 
	
### Geant 4 VMC 

To get the Geant 4 VMC sources, do 

	$ cd /opt/sw/src
	$ git clone git@github.com:vmc-project/geant4_vmc.git
	
To build Geant 4 VMC, do 

	$ cd /opt/sw/build
	$ mkdir geant4_vmc 
	$ cd geant4_vmc 
    $ cmake ../../src/geant4_vmc -DCMAKE_INSTALL_PREFIX=/opt/sw/inst \
                                 -DCMAKE_CXX_STANDARD=17 \
                                 -DCXX_STANDARD=17 \
                                 -DGeant4VMC_BUILD_G4Root=YES \
                                 -DGeant4VMC_BUILD_Geant4VMC=YES \
                                 -DGeant4VMC_USE_G4Root=YES \
                                 -DGeant4VMC_USE_VGM=YES \
                                 -DGeant4VMC_USE_GEANT4_UI=YES \
                                 -DGeant4VMC_USE_GEANT4_VIS=YES \
                                 -DGeant4VMC_USE_G3TOG4=NO
	$ make 
	
To install Geant 4 VMC, do 

	$ cd /opt/sw/build/geant4_vmc 
	$ make install 
	
## This package 

### VMC backed simulation 

	$ cd vmc/data
	$ make build 
	
### G4 backed simulation 

	$ cd g4/data 
	$ make build 
	
### Test beam data 

	$ cd tb 
	
	
### Cleaning 

	cd <top>
	make [real]clean 
	
_Christian_



	

#!/bin/bash
# script for submitting jobs at 587 alf cluster

single() {
    sleep .5
    sbatch \
        -N1 \
        -n1 \
        --partition long \
        --job-name="focalstandalone" \
        ./single.sh -p $1 -e $e -n 10000
}

for e in 20 40 60 80 100 120 150 200 250 300 350 500 750 1000 ; do
    single e+
    single pi+
    single proton
    single mu+
done

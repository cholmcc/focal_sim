//____________________________________________________________________ 
//  
//  FoCAL Virtual Monte-Carlo Simulation
//  Copyright (C) 2023 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    OPCorrelate.C
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon 06 Mar 2023 09:25:38 PM CET
    @brief   Run analysis 
 */
namespace O
{
  /** Check that user isn't running this in the source directory */
  void DontShXtWhereYouEat()
  {
    std::ifstream in("AnaConfig.C");
    if (in) {
      Error("Simulate",
	    "Do not run the simulation directly from the source directory. "
	    "Use the provided data directory or some other directory.");
      gApplication->Terminate();
    }
  }
}
/** Run analysis on the given input file

    @param nev   Number of events to analyse, < 0 means all
    @param input Input file to analyse 

    Alternatively one can run the `fwmain` program like

        fwmain -n nev LoadOP.C OPConfig.C\(\"input\"\)
 */
void
OPCorrelate(Int_t nev=-1, const char* input="events.root",bool show=false)
{
  O::DontShXtWhereYouEat();
  
  if (nev > 0 and not show) gROOT->SetBatch(kTRUE);

  gROOT->Macro("LoadOP.C");
  gROOT->Macro(Form("OPConfig.C(\"%s\")",input));

  if (nev == 0) 
    Printf("Execute \n\n"
	   "\tFramework::Main* main = Framework::Main::Instance();\n\n"
	   "to get handle on main interface.  Run 10 events by executing\n\n"
	   "\tmain->Loop(10);\n");
  else {
    gROOT->ProcessLine(Form("Framework::Main::Instance()->Loop(%d)",nev));
  }	
}
//
// EOF
//

#!/bin/bash

particle=pi+
energy=80
nev=1

usage() {
    cat <<-EOF
	Usage: $0 [OPTIONS] [-- [focal OPTIONS]]

	Options:
	  -h,--help	       This help
	  -p,--particle NAME   Particle to shoot
	  -e,--energy GEV      Energy of particle
	  -n,--n-events	       Number of events
	  --		       Send remaining arguments to 'focal' 
	EOF
}

while test $# -gt 0 ; do
    case $1 in
        -h|--help) usage $0 ; exit 0 ;; 
        -p|--particle) particle=$2 ; shift ;;
        -e|--energy)   energy=$2 ; shift ;;
        -n|--n-events) nev=$2 ; shift ;;
        --) shift ; break ;;
        *) echo "$0: Unknown option $1" > /dev/stderr ; exit 1
    esac
    shift
done

s1=$RANDOM
s2=$RANDOM
mn=`mktemp -p . gun_${particle}_${energy}_${s1}_${s2}_XXX.mac`
on=`mktemp -p . hits_${particle}_${energy}_${s1}_${s2}_XXX.root` 
cat > $mn <<EOF
/gun/particle $particle
/gun/energy $energy GeV
/run/beamOn $nev
EOF

./focal $@ -b $mn -o $on

    

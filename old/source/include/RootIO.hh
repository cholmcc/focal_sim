#ifndef INCLUDE_ROOTIO_HH
#define INCLUDE_ROOTIO_HH 1

// Include files
#include <vector>
#include <utility> // std::pair from here

#include "TROOT.h"
#include "TTree.h"
#include "TFile.h"
#include "TSystem.h"
#include "TClonesArray.h"

#include "EventAction.hh"
#include "constants.hh"

using namespace std;

/// Root IO implementation for the persistency example
class RootIO
{
public:
  virtual ~RootIO() { Close(); }

  static RootIO* GetInstance();
  void WriteEvent(int e);
  void WritePad(int i, float a);
  void WriteAlpide(int ialpide, int i, float a);
  void WriteScint(int iModule, int itower, float a);  

  void WriteParticleKinematics(float px, float py, float pz, float en);
  void WriteVertex(float x, float y, float z);
  void Fill();
  void Clear();
  void Close();
  bool Open(const std::string& filename);

protected:
  RootIO() {}

private:

  int event = 0;
  float data_pad[NpadX*NpadY*NumberPAD];
  vector<pair<int, float>> data_alpide[NalpideLayer*NumberPixRow*NumberPixCol*NumberPIX];
  vector<pair<int, float>> data_scint[N_HCAL_Module_X*N_HCAL_Module_Y];

  float particle_px = 0;
  float particle_py = 0;
  float particle_pz = 0;
  float particle_en = 0;
  float vertex_x = 0;
  float vertex_y = 0;
  float vertex_z = 0;
  TTree* fHitTree = nullptr;
  int fNevents = 0;

};
#endif // INCLUDE_ROOTIO_HH
